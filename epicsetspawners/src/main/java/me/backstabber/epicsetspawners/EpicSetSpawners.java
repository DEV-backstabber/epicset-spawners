package me.backstabber.epicsetspawners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;


import me.backstabber.epicsetspawners.api.EpicSetSpawnersAPI;
import me.backstabber.epicsetspawners.listeners.BlockListener;
import me.backstabber.epicsetspawners.listeners.GuiListener;
import me.backstabber.epicsetspawners.listeners.WildStackerListener;
import me.backstabber.epicsetspawners.stores.location.EpicLocationStore;
import me.backstabber.epicsetspawners.stores.spawner.EpicSpawnerStore;
import me.backstabber.epicsetspawners.utils.YamlManager;

public class EpicSetSpawners extends JavaPlugin {

//	@Inject
//	private SpawnerStore store;
//	@Inject
//	private LocationStore locations;
//	@Inject
//	private StackedBlocks blocks;
//	@Inject
//	private BlockListener blockListener;
//	@Inject
//	private GuiListener guiListener;
//	@Inject
//	private StackersHook stackersHook;
//	@Inject
//	private SpawnerCommand command;
//	@Inject
//	private CreateCommand create;
//	@Inject
//	private WildStackerListener wildListener;
	private YamlManager config;
	private Dependable module;

	private static EpicSetSpawnersAPI api;
	@Override
	public void onEnable() {
		config = new YamlManager(this, "config");
		module = new Dependable(null);
		module.init(this);
		module.stackersHook.setup();
		((EpicSpawnerStore) module.spawnerStore).setup();
		//load these after all worlds are loaded
		((EpicLocationStore) module.locationStore).setup();
		module.stackedBlocks.setup();
		Bukkit.getPluginManager().registerEvents(new GuiListener(this), this);
		Bukkit.getPluginManager().registerEvents(new BlockListener(this), this);
		Bukkit.getPluginManager().registerEvents(module.createCommand, this);
		if (Bukkit.getPluginManager().isPluginEnabled("WildStacker")) {
			Bukkit.getPluginManager().registerEvents(new WildStackerListener(this), this);
		}
		getCommand("espawner").setExecutor(module.spawnerCommand);
		getCommand("espawner").setTabCompleter(module.spawnerCommand);
		api = new ApiImpl();
		((ApiImpl) api).setup(this, module.stackersHook, module.spawnerStore, module.locationStore);
	}
	@Override
	public void onDisable() {
		for(Player player:Bukkit.getOnlinePlayers()) {
			if(player.hasMetadata("EpicSpawnerBuilding"))
				player.removeMetadata("EpicSpawnerBuilding", this);
		}
		if(module!=null)
			module.hologramStore.removeAllHolograms();
	}
	void injectMembers(Object object) {
		module.injectMembers(object);
	}
	public YamlManager getSettings() {
		return config;
	}

	public static EpicSetSpawnersAPI getApi() {
		return api;
	}
	public Dependable getModule() {
		return module;
	}
}
