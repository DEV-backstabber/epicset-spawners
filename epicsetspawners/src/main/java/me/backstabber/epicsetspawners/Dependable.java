package me.backstabber.epicsetspawners;


import me.backstabber.epicsetspawners.api.EpicSetSpawnersAPI;
import me.backstabber.epicsetspawners.api.stores.location.LocationStore;
import me.backstabber.epicsetspawners.api.stores.spawner.SpawnerStore;
import me.backstabber.epicsetspawners.api.stores.spawner.VanillaStore;
import me.backstabber.epicsetspawners.command.SpawnerCommand;
import me.backstabber.epicsetspawners.command.subcommand.CreateCommand;
import me.backstabber.epicsetspawners.hooks.StackersHook;
import me.backstabber.epicsetspawners.listeners.EntityListener;
import me.backstabber.epicsetspawners.listeners.ItemListener;
import me.backstabber.epicsetspawners.nms.MappedMethods;
import me.backstabber.epicsetspawners.nms.NmsMethods;
import me.backstabber.epicsetspawners.nms.impl.Version1_10;
import me.backstabber.epicsetspawners.nms.impl.Version1_11;
import me.backstabber.epicsetspawners.nms.impl.Version1_12;
import me.backstabber.epicsetspawners.nms.impl.Version1_13;
import me.backstabber.epicsetspawners.nms.impl.Version1_14;
import me.backstabber.epicsetspawners.nms.impl.Version1_15;
import me.backstabber.epicsetspawners.nms.impl.Version1_16;
import me.backstabber.epicsetspawners.nms.impl.Version1_8;
import me.backstabber.epicsetspawners.nms.impl.Version1_9;
import me.backstabber.epicsetspawners.stores.location.EpicLocationStore;
import me.backstabber.epicsetspawners.stores.location.HologramStore;
import me.backstabber.epicsetspawners.stores.location.StackedBlocks;
import me.backstabber.epicsetspawners.stores.spawner.BlockStore;
import me.backstabber.epicsetspawners.stores.spawner.CustomStore;
import me.backstabber.epicsetspawners.stores.spawner.EpicSpawnerStore;
import me.backstabber.epicsetspawners.stores.spawner.EpicVanillaStore;
import me.backstabber.epicsetspawners.stores.spawner.ItemStore;
import me.backstabber.epicsetspawners.stores.spawner.VariableStore;
import me.backstabber.epicsetspawners.utils.ReflectionUtils;

public class Dependable {

	protected EpicSetSpawners plugin;
	protected SpawnerStore spawnerStore;
	protected VanillaStore vanillaStore;
	protected ItemStore itemStore;
	protected CustomStore customStore;
	protected VariableStore variableStore;
	protected BlockStore blockStore;
	protected LocationStore locationStore;
	protected ItemListener itemListener;
	protected EntityListener entityListener;
	protected StackersHook stackersHook;
	protected EpicSetSpawnersAPI api;
	protected HologramStore hologramStore;
	
	protected NmsMethods nmsMethods;
	
	protected CreateCommand createCommand;
	protected SpawnerCommand spawnerCommand;
	protected StackedBlocks stackedBlocks;
	public Dependable(EpicSetSpawners plugin)
	 {
	     if(plugin!=null)
	    	 plugin.injectMembers(this);
	 }
	 public void init(EpicSetSpawners plugin) {
		 this.plugin = plugin;
		 this.vanillaStore = new EpicVanillaStore();
		 this.itemStore = new ItemStore();
		 this.customStore = new CustomStore();
		 this.variableStore = new VariableStore();
		 this.blockStore = new BlockStore();
		 this.spawnerStore = new EpicSpawnerStore(plugin);
		 this.locationStore = new EpicLocationStore(plugin);
		 this.itemListener = new ItemListener(plugin);
		 this.entityListener = new EntityListener(plugin);
		 this.stackersHook = new StackersHook(plugin);
		 this.api = new ApiImpl();
		if (ReflectionUtils.SERVER_VERSION < 9)
			nmsMethods = new Version1_8(stackersHook);
		else if (ReflectionUtils.SERVER_VERSION < 10)
			nmsMethods = new Version1_9(stackersHook);
		else if (ReflectionUtils.SERVER_VERSION < 11)
			nmsMethods = new Version1_10(stackersHook);
		else if (ReflectionUtils.SERVER_VERSION < 12)
			nmsMethods = new Version1_11(stackersHook);
		else if (ReflectionUtils.SERVER_VERSION < 13)
			nmsMethods = new Version1_12(stackersHook);
		else if (ReflectionUtils.SERVER_VERSION < 14)
			nmsMethods = new Version1_13(stackersHook);
		else if (ReflectionUtils.SERVER_VERSION < 15)
			nmsMethods = new Version1_14(stackersHook);
		else if (ReflectionUtils.SERVER_VERSION < 16)
			nmsMethods = new Version1_15(stackersHook);
		else if (ReflectionUtils.SERVER_VERSION < 17)
			nmsMethods = new Version1_16(stackersHook);
		else 
			nmsMethods = new MappedMethods(stackersHook);
		this.createCommand = new CreateCommand(plugin, spawnerStore, nmsMethods);
		this.spawnerCommand = new SpawnerCommand(plugin, spawnerStore, nmsMethods, locationStore, createCommand);
		this.hologramStore=new HologramStore(plugin);
		this.stackedBlocks = new StackedBlocks(plugin);
	 }
	 /**
	  * Function to inject all dependencies to
	  * an object 
	  * @param instance
	  */
	 public void injectMembers(Object instance) 
	 {
		 if(instance instanceof Dependable) {
			Dependable injectable=(Dependable) instance;
			injectable.plugin=this.plugin;
			injectable.spawnerStore=this.spawnerStore;
			injectable.vanillaStore=this.vanillaStore;
			injectable.itemStore=this.itemStore;
			injectable.customStore=this.customStore;
			injectable.variableStore=this.variableStore;
			injectable.blockStore=this.blockStore;
			injectable.locationStore=this.locationStore;
			injectable.itemListener=this.itemListener;
			injectable.entityListener=this.entityListener;
			injectable.stackersHook=this.stackersHook;
			injectable.api=this.api;
			
			injectable.nmsMethods=this.nmsMethods;
			
			injectable.createCommand=this.createCommand;
			injectable.spawnerCommand=this.spawnerCommand;
			injectable.stackedBlocks=this.stackedBlocks;
			injectable.hologramStore=this.hologramStore;
		 }
	 }
	
//	public DepInjectorMod(EpicSetSpawners plugin) {
//		this.plugin = plugin;
//		this.injector=Guice.createInjector(this);
//	}
//	public void injectMembers(Object object) {
//		injector.injectMembers(object);;
//	}
//	
//	
//	@Override
//	protected void configure() {
//		this.bind(EpicSetSpawners.class).toInstance(plugin);
//		EpicSpawnerStore store = new EpicSpawnerStore();
//		this.bind(SpawnerStore.class).toInstance(store);
//		this.bind(EpicSpawnerStore.class).toInstance(store);
//		this.bind(VanillaStore.class).toInstance(new EpicVanillaStore());
//		this.bind(ItemStore.class).toInstance(new ItemStore());
//		this.bind(CustomStore.class).toInstance(new CustomStore());
//		this.bind(VariableStore.class).toInstance(new VariableStore());
//		this.bind(BlockStore.class).toInstance(new BlockStore());
//		EpicLocationStore locations = new EpicLocationStore();
//		this.bind(LocationStore.class).toInstance(locations);
//		this.bind(EpicLocationStore.class).toInstance(locations);
//		this.bind(ItemListener.class).toInstance(new ItemListener());
//		this.bind(EntityListener.class).toInstance(new EntityListener());
//		StackersHook hook = new StackersHook();
//		this.bind(StackersHook.class).toInstance(stackersHook);
//		ApiImpl api = new ApiImpl();
//		this.bind(ApiImpl.class).toInstance(api);
//		this.bind(EpicSetSpawnersAPI.class).toInstance(api);
//		NmsMethods nmsMethods;
//		if (ReflectionUtils.SERVER_VERSION() < 9)
//			nmsMethods = new Version1_8(stackersHook);
//		else if (ReflectionUtils.SERVER_VERSION() < 10)
//			nmsMethods = new Version1_9(stackersHook);
//		else if (ReflectionUtils.SERVER_VERSION() < 11)
//			nmsMethods = new Version1_10(stackersHook);
//		else if (ReflectionUtils.SERVER_VERSION() < 12)
//			nmsMethods = new Version1_11(stackersHook);
//		else if (ReflectionUtils.SERVER_VERSION() < 13)
//			nmsMethods = new Version1_12(stackersHook);
//		else if (ReflectionUtils.SERVER_VERSION() < 14)
//			nmsMethods = new Version1_13(stackersHook);
//		else if (ReflectionUtils.SERVER_VERSION() < 15)
//			nmsMethods = new Version1_14(stackersHook);
//		else if (ReflectionUtils.SERVER_VERSION() < 16)
//			nmsMethods = new Version1_15(stackersHook);
//		else
//			nmsMethods = new Version1_16(stackersHook);
//		this.bind(NmsMethods.class).toInstance(nmsMethods);
//		CreateCommand create=new CreateCommand(plugin, store, nmsMethods);
//		SpawnerCommand command = new SpawnerCommand(plugin, store,nmsMethods, locations, create);
//		this.bind(SpawnerCommand.class).toInstance(command);
//		this.bind(CreateCommand.class).toInstance(create);
//		this.bind(StackedBlocks.class).toInstance(new StackedBlocks());
//	}
	 public StackedBlocks getStackedBlocks() {
		return stackedBlocks;
	}
}
