package me.backstabber.epicsetspawners.stores.location;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.scheduler.BukkitRunnable;

import me.backstabber.epicsetspawners.Dependable;
import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.utils.ColorUtils;
import me.backstabber.epicsetspawners.utils.CommonUtils;
import me.backstabber.epicsetspawners.utils.YamlManager;
import me.filoghost.holographicdisplays.api.HolographicDisplaysAPI;
import me.filoghost.holographicdisplays.api.hologram.Hologram;

public class HologramStore extends Dependable {
	private YamlManager file;
	private HolographicDisplaysAPI api;
	private Map<Location, ArmorStand> savedStands = new HashMap<Location, ArmorStand>();
	public HologramStore(EpicSetSpawners plugin) {
		super(plugin);
		file = new YamlManager(plugin, "HologramStore");
		if(Bukkit.getPluginManager().isPluginEnabled("HolographicDisplays")) {
			api = HolographicDisplaysAPI.get(plugin);
		}
		//loadHolograms();
		new BukkitRunnable() {
			
			@Override
			public void run() {
				removeUnwantedHolograms(1);
			}
		}.runTaskTimer(plugin, 20, 20*30);
	}
	public void updateHologram(Location location,String text) {
		updateHologram(location, text, true);
		
	}

	private void updateHologram(Location location,String text,boolean save) {
		normalize(location);
		if(api!=null) {
			fixLocation(location);
			//check if hologram exists on this location
			Hologram hologram = getHologramAt(location);
			if(hologram==null)
				hologram = api.createHologram(location);
			hologram.getLines().clear();
			hologram.getLines().appendText(ColorUtils.applyColor(text));
			if(save)
				updateFile(hologram.getPosition().toLocation(), text);
		}
		else {
			ArmorStand stand = savedStands.get(location);
			if(stand==null)
				stand = createStand(location);
			stand.setCustomName(ColorUtils.applyColor(text));
			savedStands.put(location, stand);
			if(save)
				updateFile(stand.getLocation(), text);
		}
		
	}
	public void deleteHologram(Location location) {
		normalize(location);
		if(api!=null) {
			fixLocation(location);
			//check if hologram exists on this location
			Hologram hologram = getHologramAt(location);
			if(hologram!=null) {
				deleteFile(hologram.getPosition().toLocation());
				hologram.delete();
			}
		}
		else {
			ArmorStand stand = savedStands.get(location);
			if(stand!=null) {
				deleteFile(stand.getLocation());
				savedStands.remove(stand.getLocation());
				stand.remove();
			}
			
		}
	}
	public void removeUnwantedHolograms(int radius) {
		if(api!=null) {
			for(Hologram hologram:api.getHolograms()) {
				if(!hasLink(hologram.getPosition().toLocation(), radius)) {
					deleteFile(hologram.getPosition().toLocation());
					hologram.delete();
				}
			}
		}
		else {
			List<Location> toRemove = new ArrayList<>();
			for(Location location:savedStands.keySet()) {
				if(!hasLink(location, radius)) {
					deleteFile(location);
					savedStands.get(location).remove();
					toRemove.add(location);
				}
			}
			for(Location location:toRemove)
				savedStands.remove(location);
		}
	}
	public void removeAllHolograms() {
		if(api!=null) {
			api.deleteHolograms();
		}
		for(ArmorStand stand:savedStands.values()) {
			stand.remove();
		}
	}
	public void loadHolograms() {
		Map<Location, String> saved = getAllFiles();
		for(Location location:saved.keySet()) {
			updateHologram(location, saved.get(location),false);
		}
	}
	private Hologram getHologramAt(Location location) {
		normalize(location);
		if(api==null) 
			return null;
		for(Hologram hologram:api.getHolograms())
			if(normalize(hologram.getPosition().toLocation()).equals(location))
				return hologram;
		return null;
	}

	private Location normalize(Location location) {
		location.setPitch(0);
		location.setYaw(0);
		return location;
	}

	private Location fixLocation(Location location) {
		location.add(0, 1.5, 0);
		return location;
	}
	private boolean hasLink(Location location,int radius) {
		for(int x = location.getBlockX()-radius;x<=location.getBlockX();x++)
			for(int y = location.getBlockY()-radius;y<=location.getBlockY();y++)
				for(int z = location.getBlockZ()-radius;z<=location.getBlockZ();z++) {
					if(   locationStore.isStored(location.getWorld().getBlockAt(x, y, z)) 
						||plugin.getModule().getStackedBlocks().isStored(location.getWorld().getBlockAt(x, y, z)))
						return true;
				}
		return false;
	}
	
	
	private void updateFile(Location location,String value) {
		file.getFile().set(CommonUtils.StringUtils.getString(location), value);
		file.save(false);
	}

	private void deleteFile(Location location) {
		file.getFile().set(CommonUtils.StringUtils.getString(location), null);
		file.save(false);
	}
	private Map<Location, String> getAllFiles() {
		Map<Location, String> result = new HashMap<Location, String>();
		for(String key : file.getFile().getKeys(false)) {
			result.put(CommonUtils.StringUtils.getLocation(key), file.getFile().getString(key));
		}
		return result;
	}
	private ArmorStand createStand(Location location) {
		ArmorStand stand = location.getWorld().spawn(location, ArmorStand.class);
		stand.setVisible(false);
		stand.setGravity(false);
		stand.setSmall(true);
		stand.setBasePlate(false);
		stand.setOp(true);
		stand.setInvulnerable(true);
		stand.setCustomNameVisible(true);
		return stand;
	}
}
