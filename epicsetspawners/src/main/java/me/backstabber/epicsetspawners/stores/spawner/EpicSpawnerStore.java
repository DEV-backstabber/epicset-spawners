package me.backstabber.epicsetspawners.stores.spawner;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.EntityType;

import me.backstabber.epicsetspawners.Dependable;
import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder;
import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder.SpawnerType;
import me.backstabber.epicsetspawners.api.data.SpawnerData;
import me.backstabber.epicsetspawners.api.stores.spawner.SpawnerStore;
import me.backstabber.epicsetspawners.utils.YamlManager;

public class EpicSpawnerStore extends Dependable implements SpawnerStore {


//	@Inject
//	private EpicSetSpawners plugin;
//	@Inject
//	private VanillaStore vanilla;
//	@Inject
//	private ItemStore item;
//	@Inject
//	private CustomStore custom;
//	@Inject
//	private VariableStore variable;
//	@Inject
//	private BlockStore block;

	public EpicSpawnerStore(EpicSetSpawners plugin) {
		super(plugin);
	}
	
	public void setup() {

		// Setup vanilla spawners
		for (EntityType entity : EntityType.values()) {

			if (entity.isSpawnable() && entity.isAlive()) {

				SpawnerData spawner;
				try {
					spawner=SpawnerBuilder.load(SpawnerType.VANILLA, entity.name().toLowerCase()).getSpawner();
				} catch(IllegalArgumentException e) {
					spawner=SpawnerBuilder.createDefault(SpawnerType.VANILLA, entity.name().toLowerCase()).setEntity(entity).saveToFile().getSpawner();
				}
				vanillaStore.addSpawner(entity, spawner);
			}
		}
		// setup Item spawners
		for (String fileName : getAllFiles("Spawners/Item")) {
			YamlManager file = new YamlManager(plugin, fileName, "Spawners/Item");
			try {
			SpawnerData spawner = SpawnerBuilder.load(SpawnerType.ITEM, fileName).getSpawner();
			itemStore.addSpawner(fileName, spawner);
			} catch(IllegalArgumentException e) {
				file.getLoc().delete();
			}
		}
		// setup Custom Mob Spawners
		for (String fileName : getAllFiles("Spawners/Custom Mob")) {

			YamlManager file = new YamlManager(plugin, fileName, "Spawners/Custom Mob");
			try {
				SpawnerData spawner = SpawnerBuilder.load(SpawnerType.CUSTOM_MOB, fileName).getSpawner();
				customStore.addSpawner(fileName, spawner);
			} catch(IllegalArgumentException e) {
				file.getLoc().delete();
			}
		}
		// setup Block Spawners
		for (String fileName : getAllFiles("Spawners/Block")) {

			YamlManager file = new YamlManager(plugin, fileName, "Spawners/Block");
			try {
				SpawnerData spawner = SpawnerBuilder.load(SpawnerType.BLOCK, fileName).getSpawner();
				blockStore.addSpawner(fileName, spawner);
			} catch(IllegalArgumentException e) {
				file.getLoc().delete();
			}
		}
		// TODO setup variable spawners
	}

	@Override
	public SpawnerData getSpawner(EntityType entity) {

		return vanillaStore.getSpawner(entity).clone();
	}

	@Override
	public SpawnerData getSpawner(String spawnerName) {
		spawnerName = spawnerName.toLowerCase();
		if (itemStore.isSpawner(spawnerName))
			return itemStore.getSpawner(spawnerName).clone();
		if (customStore.isSpawner(spawnerName))
			return customStore.getSpawner(spawnerName).clone();
		if (blockStore.isSpawner(spawnerName))
			return blockStore.getSpawner(spawnerName).clone();
		if (variableStore.isSpawner(spawnerName))
			return variableStore.getSpawner(spawnerName).clone();
		try {
			EntityType entity = EntityType.valueOf(spawnerName.toUpperCase());
			if (vanillaStore.isSpawner(entity))
				return vanillaStore.getSpawner(entity);
		} catch (Exception e) {
		}
		return null;
	}

	@Override
	public boolean isSpawner(String spawnerName) {

		if (itemStore.isSpawner(spawnerName))
			return true;
		if (customStore.isSpawner(spawnerName))
			return true;
		if (blockStore.isSpawner(spawnerName))
			return true;
		if (variableStore.isSpawner(spawnerName))
			return true;
		try {
			EntityType entity = EntityType.valueOf(spawnerName.toUpperCase());
			if (vanillaStore.isSpawner(entity))
				return true;
		} catch (Exception e) {
		}
		return false;
	}
	@Override
	public boolean isCustomSpawner(String spawnerName) {

		if (itemStore.isSpawner(spawnerName))
			return true;
		if (customStore.isSpawner(spawnerName))
			return true;
		if (blockStore.isSpawner(spawnerName))
			return true;
		if (variableStore.isSpawner(spawnerName))
			return true;
		return false;
	}
	
	@Override
	public boolean isSpawner(EntityType entity) {
		return vanillaStore.isSpawner(entity);
	}
	public void defaultAll() {
		for(String name:getAllSpawners()) {
			applyDefault(name);
		}
	}
	public void applyDefault(String name) {
		if(getSpawner(name)!=null) {
			SpawnerData spawner=getSpawner(name);
			SpawnerBuilder builder=SpawnerBuilder.load(spawner.getType(), name).applyDefault().saveToFile();
			switch (spawner.getType()) {

			case ITEM:
				itemStore.addSpawner(name, builder.getSpawner());
				break;

			case CUSTOM_MOB:
				customStore.addSpawner(name, builder.getSpawner());
				break;

			case BLOCK:
				blockStore.addSpawner(name, builder.getSpawner());
				break;

			case VARIABLE:
				variableStore.addSpawner(name, builder.getSpawner());
				break;

			case VANILLA:
				vanillaStore.addSpawner(EntityType.valueOf(name.toUpperCase()), builder.getSpawner());
				break;

			default:
				break;
			}
		}
	}
	public void addSpawner(String spawnerName, SpawnerBuilder builder) {
		spawnerName = spawnerName.toLowerCase();
		if (isSpawner(spawnerName))
			return;
		SpawnerData spawner = builder.setName(spawnerName).saveToFile().getSpawner();
		switch (spawner.getType()) {

		case ITEM:
			itemStore.addSpawner(spawnerName, spawner);
			break;

		case CUSTOM_MOB:
			customStore.addSpawner(spawnerName, spawner);
			break;

		case BLOCK:
			blockStore.addSpawner(spawnerName, spawner);
			break;

		case VARIABLE:
			variableStore.addSpawner(spawnerName, spawner);
			break;

		case VANILLA:
			return;

		default:
			break;
		}
	}

	public void addSpawner(String spawnerName, SpawnerData spawner) {
		spawnerName = spawnerName.toLowerCase();
		if (isSpawner(spawnerName))
			return;
		switch (spawner.getType()) {

		case ITEM:
			itemStore.addSpawner(spawnerName, spawner);
			break;

		case CUSTOM_MOB:
			customStore.addSpawner(spawnerName, spawner);
			break;

		case BLOCK:
			blockStore.addSpawner(spawnerName, spawner);
			break;

		case VARIABLE:
			variableStore.addSpawner(spawnerName, spawner);
			break;

		case VANILLA:
			return;

		default:
			break;
		}
	}

	public void removeSpawner(String spawnerName) {
		spawnerName = spawnerName.toLowerCase();
		if (itemStore.isSpawner(spawnerName))
			itemStore.removeSpawner(spawnerName);
		if (customStore.isSpawner(spawnerName))
			customStore.removeSpawner(spawnerName);
		if (blockStore.isSpawner(spawnerName))
			blockStore.removeSpawner(spawnerName);
		if (variableStore.isSpawner(spawnerName))
			variableStore.removeSpawner(spawnerName);

	}

	public List<String> getAllSpawners() {
		List<String> spawners = new ArrayList<String>();
		spawners.addAll(((EpicVanillaStore) vanillaStore).getAllSpawners());
		spawners.addAll(itemStore.getAllSpawners());
		spawners.addAll(customStore.getAllSpawners());
		spawners.addAll(blockStore.getAllSpawners());
		spawners.addAll(variableStore.getAllSpawners());
		return spawners;
	}

	public List<String> getCustomSpawners() {
		List<String> spawners = new ArrayList<String>();
		spawners.addAll(itemStore.getAllSpawners());
		spawners.addAll(customStore.getAllSpawners());
		spawners.addAll(blockStore.getAllSpawners());
		spawners.addAll(variableStore.getAllSpawners());
		return spawners;
	}

	/*
	 * Purpose of this method is to fetch all files stored in the /plugin folder
	 */
	public List<String> getAllFiles(String path) {
		File folder = new File(plugin.getDataFolder() + "/" + path);
		String[] fileNames = folder.list();
		ArrayList<String> names = new ArrayList<String>();
		if (fileNames != null) {
			for (String s : fileNames) {
				names.add(s.replace(".yml", ""));
			}
		}
		return names;
	}

}
