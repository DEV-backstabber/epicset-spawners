package me.backstabber.epicsetspawners.stores.location;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.block.Block;


import me.backstabber.epicsetspawners.Dependable;
import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.utils.CommonUtils;
import me.backstabber.epicsetspawners.utils.YamlManager;
import me.backstabber.epicsetspawners.utils.materials.EpicMaterials;

public class StackedBlocks extends Dependable {

//	@Inject
//	private EpicSetSpawners plugin;
	
	public StackedBlocks(EpicSetSpawners plugin) {
		super(plugin);
	}
	
	private YamlManager file;
	private Map<Block, Integer> blocks = new HashMap<Block, Integer>();

	public void setup() {

		file = new YamlManager(plugin, "BlockStore");
		for (String locationString : file.getFile().getKeys(false)) {
			Block block = CommonUtils.StringUtils.getBlockLocation(locationString).getBlock();
			if (!block.getType().equals(EpicMaterials.valueOf("AIR").getMaterial())&&block.getType().isBlock()) {
				int amount=file.getFile().getInt(locationString);
				setBlock(block, amount);
			} else {
				file.getFile().set(locationString, null);
			}
		}
	}

	public void setBlock(Block block,int amount) {

		blocks.put(block, amount);
		addTag(block, amount);
		file.getFile().set(CommonUtils.StringUtils.getBlockString(block.getLocation()), amount);
		file.save(false);
	}
	public boolean isStored(Block block) {

		return blocks.containsKey(block);
	}

	public int fetch(Block block) {

		return blocks.get(block);
	}

	public void removeBlock(Block block) {

		blocks.remove(block);
		file.getFile().set(CommonUtils.StringUtils.getBlockString(block.getLocation()), null);
		file.save(false);
		Location standlLocation=block.getLocation().add(0.5,0,0.5);
		hologramStore.deleteHologram(standlLocation);
	}
	private void addTag(Block block,int amount) {
		if(!plugin.getSettings().getFile().getBoolean("settings.block-stacker.show-hologram"))
			return;
		Location standlLocation=block.getLocation().add(0.5,0,0.5);
		String tag=plugin.getSettings().getFile().getString("settings.block-stacker.format");
		tag=tag.replace("%block%", CommonUtils.StringUtils.format(block.getType().name()));
		tag=tag.replace("%amount%", CommonUtils.NumericsUtils.formatTwoDecimals(amount));

		hologramStore.updateHologram(standlLocation, tag);
	}
}
