package me.backstabber.epicsetspawners.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permissible;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.stores.spawner.SpawnerStore;
import me.backstabber.epicsetspawners.nms.NmsMethods;

public abstract class SubCommands {

	protected SpawnerStore store;
	protected EpicSetSpawners plugin;
	protected NmsMethods nmsMethods;

	public SubCommands(EpicSetSpawners plugin, SpawnerStore store,NmsMethods nmsMethods) {
		this.plugin = plugin;
		this.store = store;
		this.nmsMethods=nmsMethods;
	}

	public abstract void onCommandByPlayer(Player sender, String[] sub);

	public abstract void onCommandByConsole(CommandSender sender, String[] sub);

	public abstract String getName();

	public abstract List<String> getAliases();

	public abstract List<String> getCompletor(int length, String hint);

	public boolean hasPermission(Permissible sender) {
		if (sender.hasPermission("epicsetspawners.admin"))
			return true;
		if (sender.hasPermission("epicsetspawners." + getName()))
			return true;
		return false;
	}
	protected List<Integer> getNumbers(List<String> values) {
		List<Integer> result=new ArrayList<Integer>();
		for(String s:values) {
			try {
				int value=Integer.valueOf(s);
				result.add(value);
			}catch (NumberFormatException | NullPointerException e) {
				throw new NumberFormatException(s);
			}
		}
		return result;
	}
}
