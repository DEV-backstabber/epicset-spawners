package me.backstabber.epicsetspawners.command.subcommand;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.stores.spawner.SpawnerStore;
import me.backstabber.epicsetspawners.command.SubCommands;
import me.backstabber.epicsetspawners.nms.NmsMethods;
import me.backstabber.epicsetspawners.utils.ColorUtils;

public class ConfigCommand extends SubCommands {

	private String name = "config";

	public ConfigCommand(EpicSetSpawners plugin, SpawnerStore store ,NmsMethods nmsMethods) {
		super(plugin, store,nmsMethods);
	}

	@Override
	public void onCommandByPlayer(Player sender, String[] sub) {
		if (!hasPermission(sender)) {
			sender.sendMessage(ColorUtils.applyColor("&cYou Dont Have Permission."));
			return;
		}
		onCommandByConsole(sender, sub);
	}

	@Override
	public void onCommandByConsole(CommandSender sender, String[] sub) {
		if(sub.length>=2) {
			if(sub[1].equalsIgnoreCase("reload")) {
				plugin.getSettings().reload();
				sender.sendMessage(ColorUtils.applyColor("&6&lEpicSet-Spawners &7&l>&aSucessfully reloaded config.yml."));
				return;
			}
			if(sub[1].equalsIgnoreCase("default")) {
				plugin.getSettings().loadFromPlugin();
				sender.sendMessage(ColorUtils.applyColor("&6&lEpicSet-Spawners &7&l>&aSucessfully loaded default config.yml."));
				return;
			}
		}
		sendHelp(sender);
		return;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public List<String> getAliases() {
		List<String> alias = new ArrayList<String>();
		alias.add(name);
		return alias;
	}

	@Override
	public List<String> getCompletor(int length, String hint) {
		if (length == 2) {
			List<String> result = new ArrayList<>();
			List<String> options = Arrays.asList("reload","default");
			StringUtil.copyPartialMatches(hint, options, result);
			Collections.sort(result);
			return result;
		}

		return new ArrayList<String>();
	}

	private void sendHelp(CommandSender sender) {
		sender.sendMessage(ColorUtils.applyColor("&6&lEpicSet-Spawners &7&l>&fConfig reload usage :"));
		sender.sendMessage(ColorUtils.applyColor("&6&l>&f/espawner config reload   &7Reload the config.yml file"));
		sender.sendMessage(ColorUtils.applyColor("&6&l>&f/espawner config default   &7Load default config.yml file"));
	}
}
