package me.backstabber.epicsetspawners.api.data;

import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public interface VariableData {
	
	public void updateBlock(Block block);
	public OfflinePlayer getAttachedPlayer();
	public void setAttachedPlayer(Player player);
	public void updateSpawn(Block spawner);
	
}
