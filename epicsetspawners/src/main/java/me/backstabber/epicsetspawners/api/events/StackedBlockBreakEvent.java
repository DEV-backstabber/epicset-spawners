package me.backstabber.epicsetspawners.api.events;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;

public class StackedBlockBreakEvent extends BlockBreakEvent {

	private int breakAmount;
	private int stackAmount;
	public StackedBlockBreakEvent(Block theBlock,  Player player, int stack , int breakAmount) {
		super(theBlock, player);
		this.breakAmount=breakAmount;
		this.stackAmount=stack;
	}
	public int getBreakAmount() {
		return breakAmount;
	}
	public void setBreakAmount(int breakAmount) {
		if(breakAmount>stackAmount)
			breakAmount=stackAmount;
		this.breakAmount = breakAmount;
	}
	public int getStackAmount() {
		return stackAmount;
	}
	public void setStackAmount(int stackAmount) {
		if(stackAmount<=0)
			return;
		this.stackAmount = stackAmount;
		if(breakAmount>stackAmount)
			breakAmount=stackAmount;
	}

}
