package me.backstabber.epicsetspawners.api.builder;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder.SpawnerType;
import me.backstabber.epicsetspawners.api.builder.validate.UpgradePaths;
import me.backstabber.epicsetspawners.utils.YamlManager;



public class UpgradeBuilder {
	boolean enabled=true;
	List<LevelBuilder> levels=new ArrayList<LevelBuilder>();
	public static UpgradeBuilder create() {
		UpgradeBuilder builder=new UpgradeBuilder();
		return builder.applyDefault();
	}
	public static UpgradeBuilder load(SpawnerType type,String name) {
		YamlManager manager = type.getFile(name);
		if(!manager.getFile().isSet(UpgradePaths.ENABLED.getPath()))
			throw new IllegalArgumentException("Upgrade Data doesnt exist.");
		UpgradeBuilder builder=new UpgradeBuilder();
		builder.enabled=manager.getFile().getBoolean(UpgradePaths.ENABLED.getPath());
		ConfigurationSection section = manager.getFile().getConfigurationSection(UpgradePaths.LEVELS.getPath());
		if(section!=null) {
			for(int i=1;i<=20;i++) {
				if(section.isSet(i+".cost.money")) {
					LevelBuilder lvl=LevelBuilder.create();
					lvl.setMoneyPrice((int) section.get(i+".cost.money"));
					lvl.setXpPrice((int) section.get(i+".cost.exp"));
					lvl.setMinDelay((int) section.get(i+".upgraded-settings.min-delay"));
					lvl.setMaxDelay((int) section.get(i+".upgraded-settings.max-delay"));
					lvl.setSpawnCount((int) section.get(i+".upgraded-settings.spawn-count"));
					builder.levels.add(lvl);
				}
			}
		}
		return builder;
	}
	public UpgradeBuilder applyDefault() {
		EpicSetSpawners plugin=EpicSetSpawners.getPlugin(EpicSetSpawners.class);
		ConfigurationSection file = plugin.getSettings().getFile().getConfigurationSection("default-values");
		if(file==null||!file.isSet(UpgradePaths.ENABLED.getPath()))
			return this;
		this.enabled=file.getBoolean(UpgradePaths.ENABLED.getPath());
		ConfigurationSection section = file.getConfigurationSection(UpgradePaths.LEVELS.getPath());
		if(section!=null) {
			for(int i=2;i<=20;i++) {
				if(section.isSet(i+".cost.money")) {
					LevelBuilder lvl=LevelBuilder.create();
					lvl.setMoneyPrice((int) section.get(i+".cost.money"));
					lvl.setXpPrice((int) section.get(i+".cost.exp"));
					lvl.setMinDelay((int) section.get(i+".upgraded-settings.min-delay"));
					lvl.setMaxDelay((int) section.get(i+".upgraded-settings.max-delay"));
					lvl.setSpawnCount((int) section.get(i+".upgraded-settings.spawn-count"));
					this.addUpgrade(lvl);
				}
			}
		}
		return this;
	}
	/**
	 * Enable / Disable the upgrades
	 * @param enabled
	 * @return
	 */
	public UpgradeBuilder setEnabled(boolean enabled) {
		this.enabled=enabled;
		return this;
	}
	public UpgradeBuilder addUpgrade(LevelBuilder upgrade) {
		this.levels.add(upgrade);
		return this;
	}
	public UpgradeBuilder removeUpgrade() {
		if(this.levels.size()>1) {
			levels.remove(levels.size()-1);
		}
		return this;
	}
	void applyToFile(FileConfiguration file) {
		file.set(UpgradePaths.ENABLED.getPath(), enabled);
		file.set(UpgradePaths.LEVELS.getPath(), null);
		for(int level=0;level<levels.size();level++)
			levels.get(level).apply(level+2, file);
	}
}
