package me.backstabber.epicsetspawners.api.events;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.Cancellable;
import org.bukkit.event.block.BlockExpEvent;

import me.backstabber.epicsetspawners.api.data.SpawnerData;

public class StackedBlockSpawnEvent extends BlockExpEvent implements Cancellable {
	private SpawnerData spawner;
	private Location spawnLocation;
	private Material blockType;
	private int stackAmount;
	private boolean canceled;
	public StackedBlockSpawnEvent(Block block,SpawnerData spawner, Location location, Material material,int stackAmount) {
		super(block, 0);
		this.spawner=spawner;
		this.spawnLocation=location;
		this.blockType=material;
		this.stackAmount=stackAmount;
	}
	public Material getBlockType() {
		return blockType;
	}
	public SpawnerData getSpawner() {
		return spawner;
	}
	public Location getSpawnLocation() {
		return spawnLocation;
	}
	public int getStackAmount() {
		return stackAmount;
	}
	public void setBlockType(Material blockType) {
		if(blockType.isBlock())
			this.blockType = blockType;
	}
	public void setStackAmount(int stackAmount) {
		if(stackAmount<=0)
			stackAmount=1;
		this.stackAmount = stackAmount;
	}
	@Override
	public boolean isCancelled() {
		return canceled;
	}
	@Override
	public void setCancelled(boolean cancel) {
		this.canceled=cancel;
	}
}
