package me.backstabber.epicsetspawners;

import me.backstabber.epicsetspawners.api.EpicSetSpawnersAPI;
import me.backstabber.epicsetspawners.api.stores.location.LocationStore;
import me.backstabber.epicsetspawners.api.stores.spawner.SpawnerStore;
import me.backstabber.epicsetspawners.hooks.StackersHook;

public class ApiImpl implements EpicSetSpawnersAPI {

	private EpicSetSpawners plugin;
	private StackersHook stacker;
	private SpawnerStore store;
	private LocationStore location;

	public void setup(EpicSetSpawners plugin, StackersHook stacker, SpawnerStore store, LocationStore location) {
		this.plugin = plugin;
		this.stacker = stacker;
		this.store = store;
		this.location = location;
	}

	@Override
	public StackersHook getStacker() {
		return stacker;
	}

	@Override
	public SpawnerStore getSpawnerStore() {
		return store;
	}

	@Override
	public LocationStore getLocationStore() {
		return location;
	}

	@Override
	public EpicSetSpawners getPlugin() {
		return plugin;
	}

}
