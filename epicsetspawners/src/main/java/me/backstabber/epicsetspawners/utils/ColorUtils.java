package me.backstabber.epicsetspawners.utils;


import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import me.clip.placeholderapi.PlaceholderAPI;

public class ColorUtils extends ReflectionUtils {
	private static final Pattern hexPattern = Pattern.compile("&#" + "([A-Fa-f0-9]{6})" + "");
	private static final char COLOR_CHAR = ChatColor.COLOR_CHAR;
	public static String applyColor(String text) {
		if(USE_RGB_COLORS) {
			Matcher matcher = hexPattern.matcher(text);
			StringBuffer buffer = new StringBuffer(text.length() + 4 * 8);
	        while (matcher.find())
	        {
	            String group = matcher.group(1);
	            matcher.appendReplacement(buffer, COLOR_CHAR + "x"
	                    + COLOR_CHAR + group.charAt(0) + COLOR_CHAR + group.charAt(1)
	                    + COLOR_CHAR + group.charAt(2) + COLOR_CHAR + group.charAt(3)
	                    + COLOR_CHAR + group.charAt(4) + COLOR_CHAR + group.charAt(5)
	                    );
	        }
	        String message =  matcher.appendTail(buffer).toString();
	        return net.md_5.bungee.api.ChatColor.translateAlternateColorCodes('&', message);
        }
		return ChatColor.translateAlternateColorCodes('&', text);
	}
	public static List<String> applyColor(List<String> text) {
		List<String> result=new ArrayList<>();
		for(String s:text)
			result.add(applyColor(s));
		return result;
	}
	public static String applyColorAndPlaceholders(Player player,String text) {
		if(Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI"))
			text=PlaceholderAPI.setPlaceholders(player, text);
		return applyColor(text);
	}
	public static List<String> applyColorAndPlaceholders(Player player,List<String> text) {
		List<String> result=new ArrayList<>();
		for(String s:text)
			result.add(applyColorAndPlaceholders(player, s));
		return result;
	}
	public static boolean hasColorCode(String text) {
		return !ChatColor.translateAlternateColorCodes('&', text).equals(text);
	}
	public static boolean hasHexCode(String text) {
		if(USE_RGB_COLORS) {
			Matcher match = hexPattern.matcher(text);
            return match.find();
        }
		return false;
	}
	public static String stripColors(String text) {
		if(USE_RGB_COLORS)
			return net.md_5.bungee.api.ChatColor.stripColor(applyColor(text));
		else return ChatColor.stripColor(applyColor(text));
	}
}
