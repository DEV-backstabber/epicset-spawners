package me.backstabber.epicsetspawners.utils.materials;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;

public class EpicMaterials implements Versionable{
	private Enum<?> material;
	public static EpicMaterials valueOf(String material) {
		EpicMaterials mat=new EpicMaterials();
		if(!USE_DEFAULT_MATERIALS) 
			mat.material=UMaterials.valueOf(material);
		else 
			mat.material=Material.valueOf(material);
		return mat;
	}
	public static EpicMaterials valueOf(ItemStack item) {
		EpicMaterials mat=new EpicMaterials();
		if(!USE_DEFAULT_MATERIALS) 
			mat.material=UMaterials.match(item);
		else 
			mat.material=item.getType();
		return mat;
	}
	public static EpicMaterials valueOf(UMaterials umat) {
		EpicMaterials mat=new EpicMaterials();
		if(!USE_DEFAULT_MATERIALS) 
			mat.material=umat;
		else 
			mat.material=umat.getMaterial();
		return mat;
	}
	public static EpicMaterials valueOf(Block block) {
		EpicMaterials mat=new EpicMaterials();
		if(!USE_DEFAULT_MATERIALS) 
			mat.material=UMaterials.getItem(block);
		else 
			mat.material=block.getType();
		return mat;
	}
	public Material getMaterial() {
		return (USE_DEFAULT_MATERIALS) ? (Material) material : ((UMaterials) material).getMaterial();
	}
	public ItemStack getItemStack() {
		return (USE_DEFAULT_MATERIALS) ? new ItemStack((Material) material) : ((UMaterials) material).getItemStack();
	}
	public String name() {
		return material.name();
	}
}
