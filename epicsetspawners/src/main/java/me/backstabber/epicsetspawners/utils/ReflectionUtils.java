package me.backstabber.epicsetspawners.utils;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;


import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author DPOH-VAR
 * @version 1.0
 */
@SuppressWarnings("rawtypes")
public class ReflectionUtils {
	/**
	 * Some identifiers for current server version
	 */
	static String VERSION = Bukkit.getVersion();
	public static boolean EIGHT = VERSION.contains("1.8"), 
		NINE = VERSION.contains("1.9"), 
		TEN = VERSION.contains("1.10"), 
		ELEVEN = VERSION.contains("1.11"), 
		TWELVE = VERSION.contains("1.12"), 
		THIRTEEN = VERSION.contains("1.13"), 
		FOURTEEN = VERSION.contains("1.14"), 
		FIFTEEN = VERSION.contains("1.15"), 
		SIXTEEN=VERSION.contains("1.16") ,
		SEVENTEEN=VERSION.contains("1.17"),
		EIGHTEEN=VERSION.contains("1.18"),
		NINETEEN=VERSION.contains("1.19");
	public static boolean USE_DEFAULT_MATERIALS = SIXTEEN || SEVENTEEN || EIGHTEEN || NINETEEN;
	public static boolean USE_RGB_COLORS = SIXTEEN || SEVENTEEN || EIGHTEEN || NINETEEN;
	public static boolean USE_NEW_MAPPINGS = SEVENTEEN || EIGHTEEN || NINETEEN;
	public static double SERVER_VERSION=getVersion();;
	private static Double getVersion() {
		Pattern VERSION_PATTERN = Pattern.compile("minecraftVersion=1.[0-9.]+");
		String versionString=Bukkit.getServer().toString();
		Matcher match = VERSION_PATTERN.matcher(versionString);
		if(match.find()) {
			String result=versionString.substring(match.start()+19,match.end());
			return Double.valueOf(result);
		}
		return 19D;
	}
	
	
	/**
	 * Some methods commonly used
	 */
	
	//sending packet
	private static RefMethod sendPacketMethod;
	private static RefMethod getHandle;
	private static RefField getConnection;
	
	//string to chatcomponent
	private static RefMethod getChatJsonComponent;
	private static RefConstructor getChatBaseComponent;
	
	//chat packets / enums
	private static RefConstructor textChatPacket;
	
    private static Object enumChatMessage;
    private static Object enumActionbarMessage;
    //title / subtitle Packets / enums
	private static RefConstructor titlePacket;
	private static RefConstructor subTitlePacket;
	private static RefConstructor titleAnimattionPacket;

//	private static RefMethod setNBTString;
//	private static RefMethod getNBTString;

    private static Object enumActionTitle;
    private static Object enumActionSubtitle;
    
    
	
	
	
	
	/** prefix of bukkit classes */
	private static String preClassB = "org.bukkit.craftbukkit";
	/** prefix of minecraft classes */
	private static String preClassM = "net.minecraft.server";
	/** boolean value, TRUE if server uses forge or MCPC+ */
	private static boolean forge = false;

	/** check server version and class names */
	static {
		if (Bukkit.getServer() != null) {
			if (Bukkit.getVersion().contains("MCPC") || Bukkit.getVersion().contains("Forge"))
				forge = true;
			Server server = Bukkit.getServer();
			Class<?> bukkitServerClass = server.getClass();
			String[] pas = bukkitServerClass.getName().split("\\.");
			if (pas.length == 5) {
				String verB = pas[3];
				preClassB += "." + verB;
			}
			try {
				Method getHandle = bukkitServerClass.getDeclaredMethod("getHandle");
				Object handle = getHandle.invoke(server);
				Class handleServerClass = handle.getClass();
				pas = handleServerClass.getName().split("\\.");
				if (pas.length == 5) {
					String verM = pas[3];
					preClassM += "." + verM;
				}
			} catch (Exception ignored) {
			}
		}
		
		
		//setup Send packet methods
    	getHandle = getRefClass("{cb}.entity.CraftPlayer").getMethod("getHandle");
    	RefClass entityPlayerClass;
    	if(!USE_NEW_MAPPINGS) {
    		entityPlayerClass = getRefClass("{nms}.EntityPlayer");
    		getConnection = entityPlayerClass.findField(getRefClass("{nms}.PlayerConnection"));
    	}
    	else {
    		entityPlayerClass = getRefClass("{nm}.server.level.EntityPlayer");
    		getConnection = entityPlayerClass.findField(getRefClass("{nm}.server.network.PlayerConnection"));
    	}
    	if(!EIGHTEEN && !NINETEEN) 
    		sendPacketMethod = getRefClass(getConnection.getFieldRefClass().getRealClass()).findMethodByName("sendPacket");
    	
    	else 
    		sendPacketMethod = getRefClass(getConnection.getFieldRefClass().getRealClass()).getMethod("a", getRefClass("{nm}.network.protocol.Packet").getRealClass());
    	//string to chat methods
    	if(USE_NEW_MAPPINGS) {
    		getChatJsonComponent = getRefClass("{nm}.network.chat.IChatBaseComponent$ChatSerializer").getMethod("a", String.class);
    		if(!NINETEEN)
    			getChatBaseComponent = getRefClass("{nm}.network.chat.ChatComponentText").getConstructor(String.class);
    	}
    	else {
    		getChatJsonComponent = getRefClass("{nms}.IChatBaseComponent$ChatSerializer").getMethod("a", String.class);
			getChatBaseComponent = getRefClass("{nms}.ChatComponentText").getConstructor(String.class);
    	}
    	//chat packets
    	if(USE_NEW_MAPPINGS) {
    		titlePacket = getRefClass("{nm}.network.protocol.game.ClientboundSetTitleTextPacket").getConstructor(getRefClass("{nm}.network.chat.IChatBaseComponent").getRealClass());
    		subTitlePacket = getRefClass("{nm}.network.protocol.game.ClientboundSetSubtitleTextPacket").getConstructor(getRefClass("{nm}.network.chat.IChatBaseComponent").getRealClass());
    		titleAnimattionPacket = getRefClass("{nm}.network.protocol.game.ClientboundSetTitlesAnimationPacket").findConstructor(3);
    		if(!NINETEEN) {
    			
    			RefMethod enumMethod = getRefClass("{nm}.network.chat.ChatMessageType").getMethod("a", byte.class);
    			enumChatMessage = enumMethod.call((byte)1);
    			enumActionbarMessage = enumMethod.call((byte)2);
    			textChatPacket = getRefClass("{nm}.network.protocol.game.PacketPlayOutChat").findConstructor(3);
    			
    		}
    		else 
    			textChatPacket = getRefClass("{nm}.network.protocol.game.ClientboundSystemChatPacket").getConstructor(getRefClass("{nm}.network.chat.IChatBaseComponent").getRealClass(),boolean.class);
    	}
    	else  {
    		
    		titlePacket = getRefClass("{nms}.PacketPlayOutTitle").findConstructor(2);
    		subTitlePacket = getRefClass("{nms}.PacketPlayOutTitle").findConstructor(2);
    		titleAnimattionPacket = getRefClass("{nms}.PacketPlayOutTitle").findConstructor(3);
    		
    		enumActionTitle = getRefClass("{nms}.PacketPlayOutTitle$EnumTitleAction").getField("TITLE").of(null).get();
    		enumActionSubtitle = getRefClass("{nms}.PacketPlayOutTitle$EnumTitleAction").getField("SUBTITLE").of(null).get();
    		if(SERVER_VERSION>=12) {
	    		RefMethod enumMethod = getRefClass("{nms}.ChatMessageType").getMethod("a", byte.class);
				enumChatMessage = enumMethod.call((byte)1);
				enumActionbarMessage = enumMethod.call((byte)2);
    		}
    		if(SIXTEEN)
    			textChatPacket = getRefClass("{nms}.PacketPlayOutChat").findConstructor(3);
        	else
        		textChatPacket = getRefClass("{nms}.PacketPlayOutChat").findConstructor(2);
    	}
//    	if(USE_NEW_MAPPINGS) 
//    		setNBTString = (SERVER_VERSION <=17)
//    				?getRefClass("{nm}.nbt.NBTTagCompound").getMethod("setString", String.class,String.class)
//    				:getRefClass("{nm}.nbt.NBTTagCompound").getMethod("a", String.class,String.class);
//    	else
//    		setNBTString =getRefClass("{nms}.NBTTagCompound").getMethod("setString", String.class,String.class);
//    	if(USE_NEW_MAPPINGS) 
//    		getNBTString = (SERVER_VERSION <=17)
//    				?getRefClass("{nm}.nbt.NBTTagCompound").getMethod("getString", String.class)
//    				:getRefClass("{nm}.nbt.NBTTagCompound").getMethod("l", String.class);
//    	else
//    		setNBTString =getRefClass("{nms}.NBTTagCompound").getMethod("setString", String.class);
	}
	
	
	public static void sendPacket(Object packet, Player... players) {
        if (packet == null) {
            return;
        }
        for (Player player : players) {
        	sendPacketMethod.of(getConnection.of(getHandle.of(player).call()).get()).call(packet);
        }

    }
	public static Object createActionbarPacket(String message) {
    	if(USE_NEW_MAPPINGS) {
    		if(!NINETEEN) 
    			return textChatPacket.create(fromJson(message),enumActionbarMessage,UUID.randomUUID());
    		else
    			return textChatPacket.create(fromJson(message),true);
		}
    	else if(SIXTEEN)
			return textChatPacket.create(fromJson(message),enumActionbarMessage,UUID.randomUUID());
    	else if(SERVER_VERSION<12)
    		return textChatPacket.create(fromJson(message),(byte)2);
    	else
    		return textChatPacket.create(fromJson(message),enumActionbarMessage);
    }

	public static Object createTextPacket(String message) {
		if(USE_NEW_MAPPINGS) {
    		if(!NINETEEN) 
    			return textChatPacket.create(fromJson(message),enumChatMessage,UUID.randomUUID());
    		else
    			return textChatPacket.create(fromJson(message),false);
		}
    	else if(SIXTEEN)
			return textChatPacket.create(fromJson(message),enumChatMessage,UUID.randomUUID());
    	else if(SERVER_VERSION<12)
    		return textChatPacket.create(fromJson(message),(byte)1);
    	else
    		return textChatPacket.create(fromJson(message),enumChatMessage);
    }

	public static Object createTitlePacket(String message) {
    	if(USE_NEW_MAPPINGS)
    		return titlePacket.create(fromJson(message));
    	else
    		return titlePacket.create(enumActionTitle,fromJson(message));
    }

	public static Object createSubtitlePacket(String message) {
    	if(USE_NEW_MAPPINGS)
    		return subTitlePacket.create(fromJson(message));
    	else
    		return subTitlePacket.create(enumActionSubtitle,fromJson(message));
    	
    }

	public static Object createTitleTimesPacket(int fadeIn, int stay, int fadeOut) {
    	return titleAnimattionPacket.create(fadeIn,stay,fadeOut);
    }

    
	private static Object componentText(String message) {
    	if(!NINETEEN)
            return getChatBaseComponent.create(message);
    	return getChatJsonComponent.call(message);
    }

    
    private static Object fromJson(String json) {
        if (!json.trim().startsWith("{")) {
            return componentText(json);
        }
        return getChatJsonComponent.call(json);
    }
	
    /**
	 * Method to convert nmsItemstack to bukkitItemstack
	 */
	public static Object getNMSItemStack(ItemStack item) {
		return getRefClass("{cb}.inventory.CraftItemStack").getMethod("asNMSCopy",
				ItemStack.class).call(item);
	}

	/**
	 * Method to convert nmsItemstack to bukkitItemstack
	 */
	public static ItemStack getItemStack(Object nmsItem) {
		return (ItemStack) getRefClass("{cb}.inventory.CraftItemStack").getMethod("asBukkitCopy",
				nmsItem.getClass()).call(nmsItem);
	}
	public static ItemStack fromNbtString(String nbtTag) {
		Object tag;
		if(USE_NEW_MAPPINGS) {
			if(SEVENTEEN) {
				tag = getRefClass("{nm}.nbt.MojangsonParser").getMethod("parse", String.class).call(nbtTag);
			}
			else {
				tag = getRefClass("{nm}.nbt.MojangsonParser").getMethod("a", String.class).call(nbtTag);
			}
		}
		else {
			tag = getRefClass("{nms}.MojangsonParser").getMethod("parse", String.class).call(nbtTag);
		}
		return fromNBT(tag);
	}
	public static String toNBTString(ItemStack item) {
		return toNBT(item).toString();
	}

	public static Object toNBT(ItemStack item) {
		Object nmsItem = getNMSItemStack(item);
		RefClass nbtTagCompound;
		if(USE_NEW_MAPPINGS) {
			nbtTagCompound=getRefClass("{nm}.nbt.NBTTagCompound");
		}
		else
			nbtTagCompound=getRefClass("{nms}.NBTTagCompound");
		RefMethod saveNbt;
		if(USE_NEW_MAPPINGS) {
			if(SEVENTEEN) {
				saveNbt = getRefClass("{nm}.world.item.ItemStack").getMethod("save", nbtTagCompound.getRealClass());
			}
			else {
				saveNbt = getRefClass("{nm}.world.item.ItemStack").getMethod("b", nbtTagCompound.getRealClass());
			}
		}
		else {
			saveNbt = getRefClass("{nms}.ItemStack").getMethod("save",
					nbtTagCompound.getRealClass());
		}
		Object tag = nbtTagCompound.getConstructor().create();
		saveNbt.of(nmsItem).call(tag);
		return tag;
	}
	public static ItemStack fromNBT(Object tag) {
		Object itemStack;
		if(EIGHT || NINE || TEN || ELEVEN) {
			itemStack = getRefClass("{nms}.ItemStack").getMethod("createStack", tag.getClass()).call(tag);
		}
		else if(TWELVE) {
			itemStack = getRefClass("{nms}.ItemStack").getConstructor(tag.getClass()).create(tag);
		}
		else if(!USE_NEW_MAPPINGS) {
			itemStack = getRefClass("{nms}.ItemStack").getMethod("a", tag.getClass()).call(tag);
		}
		else {
			itemStack = getRefClass("{nm}.world.item.ItemStack").getMethod("a", tag.getClass()).call(tag);
		}
		return getItemStack(itemStack);
	}

	public static String toNBTString(Entity entity) {
		Object nmsEntity=getRefClass("{cb}.entity.CraftEntity").getMethod("getHandle").of(entity).call();

		RefClass nbtTagCompound;
		if(USE_NEW_MAPPINGS) {
			nbtTagCompound=getRefClass("{nm}.nbt.NBTTagCompound");
		}
		else
			nbtTagCompound=getRefClass("{nms}.NBTTagCompound");
		RefMethod saveNbt;

		if(SERVER_VERSION<12)
			saveNbt=getRefClass(nmsEntity.getClass()).getMethod("e", nbtTagCompound.getRealClass());
		else if(SERVER_VERSION<18)
			saveNbt=getRefClass(nmsEntity.getClass()).getMethod("save", nbtTagCompound.getRealClass());
		else if(EIGHTEEN)
			saveNbt=getRefClass(nmsEntity.getClass()).getMethod("f", nbtTagCompound.getRealClass());
		else
			saveNbt=getRefClass(nmsEntity.getClass()).getMethod("g", nbtTagCompound.getRealClass());
		Object tag = nbtTagCompound.getConstructor().create();
		saveNbt.of(nmsEntity).call(tag); 
		return tag.toString();
	}
	public static ItemStack addNbtTag(ItemStack item,String key,String value) {
//		Object tag = toNBT(item);
//		setNBTString.of(tag).call(key,value);
//		ItemStack tagged = fromNBT(tag);
		return NBTEditor.set(item, value, key);
	}
	public static String getNbtTag(ItemStack item,String key) {
//		Object tag = toNBT(item);
//		System.out.println(tag.toString());
//		String value =  getNBTString.of(tag).call(key).toString();
		String tag =  NBTEditor.getString(item, key);
		return tag;
	}
	/**
	 * @return true if server has forge classes
	 */
	public static boolean isForge() {
		return forge;
	}

	/**
	 * Get class for name. Replace {nms} to net.minecraft.server.V*. Replace {cb} to
	 * org.bukkit.craftbukkit.V*. Replace {nm} to net.minecraft
	 * 
	 * @param classes possible class paths
	 * @return RefClass object
	 * @throws RuntimeException if no class found
	 */
	public static RefClass getRefClass(String... classes) {
		for (String className : classes)
			try {
				className = className.replace("{cb}", preClassB).replace("{nms}", preClassM).replace("{nm}",
						"net.minecraft");
				return getRefClass(Class.forName(className));
			} catch (ClassNotFoundException ignored) {
			}
		throw new RuntimeException("no class found");
	}

	/**
	 * get RefClass object by real class
	 * 
	 * @param clazz class
	 * @return RefClass based on passed class
	 */
	public static RefClass getRefClass(Class clazz) {
		return new RefClass(clazz);
	}

	/**
	 * RefClass - utility to simplify work with reflections.
	 */
	public static class RefClass {
		private final Class<?> clazz;

		/**
		 * get passed class
		 * 
		 * @return class
		 */
		public Class<?> getRealClass() {
			return clazz;
		}

		private RefClass(Class<?> clazz) {
			this.clazz = clazz;
		}

		/**
		 * see {@link Class#isInstance(Object)}
		 * 
		 * @param object the object to check
		 * @return true if object is an instance of this class
		 */
		public boolean isInstance(Object object) {
			return clazz.isInstance(object);
		}

		/**
		 * get existing method by name and types
		 * 
		 * @param name  name
		 * @param types method parameters. can be Class or RefClass
		 * @return RefMethod object
		 * @throws RuntimeException if method not found
		 */
		public RefMethod getMethod(String name, Object... types) {
			try {
				Class[] classes = new Class[types.length];
				int i = 0;
				for (Object e : types) {
					if (e instanceof Class)
						classes[i++] = (Class) e;
					else if (e instanceof RefClass)
						classes[i++] = ((RefClass) e).getRealClass();
					else
						classes[i++] = e.getClass();
				}
				try {
					return new RefMethod(clazz.getMethod(name, classes));
				} catch (NoSuchMethodException ignored) {
					return new RefMethod(clazz.getDeclaredMethod(name, classes));
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

		/**
		 * get existing constructor by types
		 * 
		 * @param types parameters. can be Class or RefClass
		 * @return RefMethod object
		 * @throws RuntimeException if constructor not found
		 */
		public RefConstructor getConstructor(Object... types) {
			try {
				Class[] classes = new Class[types.length];
				int i = 0;
				for (Object e : types) {
					if (e instanceof Class)
						classes[i++] = (Class) e;
					else if (e instanceof RefClass)
						classes[i++] = ((RefClass) e).getRealClass();
					else
						classes[i++] = e.getClass();
				}
				try {
					return new RefConstructor(clazz.getConstructor(classes));
				} catch (NoSuchMethodException ignored) {
					return new RefConstructor(clazz.getDeclaredConstructor(classes));
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

		/**
		 * find method by type parameters
		 * 
		 * @param types parameters. can be Class or RefClass
		 * @return RefMethod object
		 * @throws RuntimeException if method not found
		 */
		@SuppressWarnings("unused")
		public RefMethod findMethod(Object... types) {
			Class[] classes = new Class[types.length];
			int t = 0;
			for (Object e : types) {
				if (e instanceof Class)
					classes[t++] = (Class) e;
				else if (e instanceof RefClass)
					classes[t++] = ((RefClass) e).getRealClass();
				else
					classes[t++] = e.getClass();
			}
			List<Method> methods = new ArrayList<>();
			Collections.addAll(methods, clazz.getMethods());
			Collections.addAll(methods, clazz.getDeclaredMethods());
			findMethod: for (Method m : methods) {
				Class<?>[] methodTypes = m.getParameterTypes();
				if (methodTypes.length != classes.length)
					continue;
				for (int i = 0; i < classes.length; i++) {
					if (!classes.equals(methodTypes))
						continue findMethod;
					return new RefMethod(m);
				}
			}
			throw new RuntimeException("no such method");
		}

		/**
		 * find method by name
		 * 
		 * @param names possible names of method
		 * @return RefMethod object
		 * @throws RuntimeException if method not found
		 */
		public RefMethod findMethodByName(String... names) {
			List<Method> methods = new ArrayList<>();
			Collections.addAll(methods, clazz.getMethods());
			Collections.addAll(methods, clazz.getDeclaredMethods());
			for (Method m : methods) {
				for (String name : names) {
					if (m.getName().equals(name)) {
						return new RefMethod(m);
					}
				}
			}
			throw new RuntimeException("no such method");
		}

		/**
		 * find method by return value
		 * 
		 * @param type type of returned value
		 * @throws RuntimeException if method not found
		 * @return RefMethod
		 */
		public RefMethod findMethodByReturnType(RefClass type) {
			return findMethodByReturnType(type.clazz);
		}

		/**
		 * find method by return value
		 * 
		 * @param type type of returned value
		 * @return RefMethod
		 * @throws RuntimeException if method not found
		 */
		public RefMethod findMethodByReturnType(Class type) {
			if (type == null)
				type = void.class;
			List<Method> methods = new ArrayList<>();
			Collections.addAll(methods, clazz.getMethods());
			Collections.addAll(methods, clazz.getDeclaredMethods());
			for (Method m : methods) {
				if (type.equals(m.getReturnType())) {
					return new RefMethod(m);
				}
			}
			throw new RuntimeException("no such method");
		}

		/**
		 * find constructor by number of arguments
		 * 
		 * @param number number of arguments
		 * @return RefConstructor
		 * @throws RuntimeException if constructor not found
		 */
		public RefConstructor findConstructor(int number) {
			List<Constructor> constructors = new ArrayList<>();
			Collections.addAll(constructors, clazz.getConstructors());
			Collections.addAll(constructors, clazz.getDeclaredConstructors());
			for (Constructor m : constructors) {
				if (m.getParameterTypes().length == number)
					return new RefConstructor(m);
			}
			throw new RuntimeException("no such constructor");
		}

		/**
		 * get field by name
		 * 
		 * @param name field name
		 * @return RefField
		 * @throws RuntimeException if field not found
		 */
		public RefField getField(String name) {
			try {
				try {
					return new RefField(clazz.getField(name));
				} catch (NoSuchFieldException ignored) {
					return new RefField(clazz.getDeclaredField(name));
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

		/**
		 * find field by type
		 * 
		 * @param type field type
		 * @return RefField
		 * @throws RuntimeException if field not found
		 */
		public RefField findField(RefClass type) {
			return findField(type.clazz);
		}

		/**
		 * find field by type
		 * 
		 * @param type field type
		 * @return RefField
		 * @throws RuntimeException if field not found
		 */
		public RefField findField(Class type) {
			if (type == null)
				type = void.class;
			List<Field> fields = new ArrayList<>();
			Collections.addAll(fields, clazz.getFields());
			Collections.addAll(fields, clazz.getDeclaredFields());
			for (Field f : fields) {
				if (type.equals(f.getType())) {
					return new RefField(f);
				}
			}
			throw new RuntimeException("no such field");
		}
	}

	/**
	 * Method wrapper
	 */
	public static class RefMethod {
		private final Method method;

		/**
		 * @return passed method
		 */
		public Method getRealMethod() {
			return method;
		}

		/**
		 * @return owner class of method
		 */
		public RefClass getRefClass() {
			return new RefClass(method.getDeclaringClass());
		}

		/**
		 * @return class of method return type
		 */
		public RefClass getReturnRefClass() {
			return new RefClass(method.getReturnType());
		}

		private RefMethod(Method method) {
			this.method = method;
			method.setAccessible(true);
		}

		/**
		 * apply method to object
		 * 
		 * @param e object to which the method is applied
		 * @return RefExecutor with method call(...)
		 */
		public RefExecutor of(Object e) {
			return new RefExecutor(e);
		}

		/**
		 * call static method
		 * 
		 * @param params sent parameters
		 * @return return value
		 */
		public Object call(Object... params) {
			try {
				return method.invoke(null, params);
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}

		public class RefExecutor {
			Object e;

			public RefExecutor(Object e) {
				this.e = e;
			}

			/**
			 * apply method for selected object
			 * 
			 * @param params sent parameters
			 * @return return value
			 * @throws RuntimeException if something went wrong
			 */
			public Object call(Object... params) {
				try {
					return method.invoke(e, params);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
	}

	/**
	 * Constructor wrapper
	 */
	public static class RefConstructor {
		private final Constructor constructor;

		/**
		 * @return passed constructor
		 */
		public Constructor getRealConstructor() {
			return constructor;
		}

		/**
		 * @return owner class of method
		 */
		public RefClass getRefClass() {
			return new RefClass(constructor.getDeclaringClass());
		}

		private RefConstructor(Constructor constructor) {
			this.constructor = constructor;
			constructor.setAccessible(true);
		}

		/**
		 * create new instance with constructor
		 * 
		 * @param params parameters for constructor
		 * @return new object
		 * @throws RuntimeException if something went wrong
		 */
		public Object create(Object... params) {
			try {
				return constructor.newInstance(params);
			} catch (Exception e) {
				e.printStackTrace();
				//throw new RuntimeException(e);
			}
			return null;
		}
	}

	public static class RefField {
		private Field field;

		/**
		 * @return passed field
		 */
		public Field getRealField() {
			return field;
		}

		/**
		 * @return owner class of field
		 */
		public RefClass getRefClass() {
			return new RefClass(field.getDeclaringClass());
		}

		/**
		 * @return type of field
		 */
		public RefClass getFieldRefClass() {
			return new RefClass(field.getType());
		}

		private RefField(Field field) {
			this.field = field;
			field.setAccessible(true);
		}

		/**
		 * apply fiend for object
		 * 
		 * @param e applied object
		 * @return RefExecutor with getter and setter
		 */
		public RefExecutor of(Object e) {
			return new RefExecutor(e);
		}

		public class RefExecutor {
			Object e;

			public RefExecutor(Object e) {
				this.e = e;
			}

			/**
			 * set field value for applied object
			 * 
			 * @param param value
			 */
			public void set(Object param) {
				try {
					field.set(e, param);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}

			/**
			 * get field value for applied object
			 * 
			 * @return value of field
			 */
			public Object get() {
				try {
					return field.get(e);
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
	}

	public interface Versionable {
		
	}
}