package me.backstabber.epicsetspawners.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.WorldCreator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import me.backstabber.epicsetspawners.utils.materials.EpicMaterials;
import me.backstabber.epicsetspawners.utils.materials.UMaterials;
import net.milkbowl.vault.economy.Economy;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.*;
import java.util.Map.Entry;

public class CommonUtils {
	/**
	 * Utility class for date & time based operations
	 * @author Mugha
	 *
	 */
	public static class TemporalUtils {
		/**
		 * Get the current year
		 * @return
		 */
		public static String getCurrentYear() {
			return String.valueOf(Year.now().getValue());
		}
		/**
		 * Get the current month
		 * @return
		 */
		public static String getCurrentMonth() {
			Calendar cal = Calendar.getInstance();
			return (new SimpleDateFormat("MMM").format(cal.getTime()));
		}
		public static String getFormattedTime() {
			return getformatedTime(System.currentTimeMillis()/1000D, null);
		}
		/**
		 * formats the time into dd days HH hours mm min
		 * or HH hours mm min ss sec
		 * or mm min ss sec
		 * depending on the time given
		 * @param seconds
		 * @return string representing accurate time
		 */
		public static String getFormattedTime(double seconds) {
			return getformatedTime(seconds, null);
		}
		/**
		 * formats the time into dd days HH hours mm min
		 * or HH hours mm min ss sec
		 * or mm min ss sec
		 * depending on the time given
		 * @param timeInSeconds
		 * @param node of a configutarion containing format prefabs
		 * @return string representing accurate time
		 */
		public static String getformatedTime(double timeInSeconds,ConfigurationSection node) {
			String total = "";
			int day = (int) toDays(timeInSeconds);
			int hour = (int) toHours(timeInSeconds);
			int min = (int) toMin(timeInSeconds);
			int sec = toSec(timeInSeconds);
			if (timeInSeconds >= 86400) { // atleast a day
				if(node!=null&&node.isSet("format-days"))
					total=node.getString("format-days").replace("%second%", sec+"").replace("%minute%", min+"").replace("%hour%", hour+"").replace("%day%", day+"");
				else
					total = day + " days " + hour + " hours " + min + " min";
			} 
			else if (timeInSeconds >= 3600) { // atleast an hour
				if(node!=null&&node.isSet("format-hours"))
					total=node.getString("format-hours").replace("%second%", sec+"").replace("%minute%", min+"").replace("%hour%", hour+"").replace("%day%", day+"");
				else
					total = hour + " hours " + min + " min " + sec + " sec";
			} 
			else { // a min or less
				if(node!=null&&node.isSet("format-minutes"))
					total=node.getString("format-minutes").replace("%second%", sec+"").replace("%minute%", min+"").replace("%hour%", hour+"").replace("%day%", day+"");
				else
					total = hour + " hours " + min + " min " + sec + " sec";
				total = min + " min " + sec + " sec";
			}

			return total;
		}
		public static String getFormattedTime(int seconds) {
			return getformatedTime((double)seconds,null);
		}
		public static String getFormattedTime(int seconds,ConfigurationSection node) {
			return getformatedTime((double)seconds, node);
		}
		
		// extra functions for above logic
		private static int toSec(double total) {
			double temp = ((total / 86400.0D - toDays(total)) * 24.0D - toHours(total)) * 60 - toMin(total);
			return (int) Math.floor(temp * 60);
		}
		private static double toMin(double total) {
			double temp = (total / 86400.0D - toDays(total)) * 24.0D - toHours(total);
			return Math.floor(temp * 60.0D);
		}
		private static double toHours(double total) {
			double temp = total / 86400.0D - toDays(total);
			return Math.floor(temp * 24.0D);
		}
		private static double toDays(double total) {
			return Math.floor(total / 86400.0D);
		}
	}
	
	
	/**
	 * Utility Class for all Bukkit & Spigot related operations
	 * @author Mugha
	 *
	 */
	public static class BukkitUtils {
		/**
		 * Check if a file is a valid yml file
		 * @param ymlFile
		 * @return
		 */
		public static boolean checkYmlFile(File ymlFile) {
			YamlConfiguration yamlConfiguration = new YamlConfiguration();
			try {
				yamlConfiguration.load(ymlFile);
				return true;
			} catch (IOException e) {
				return false;
			} catch (InvalidConfigurationException e) {
				return false;
			}
		}
		/**
		 * Get the current server version in the form of a double number
		 * the number represents the major version followed by the minor version
		 * for example 1.12.2 is interpreted as 12.2.
		 * @return server version
		 */
		public static double getServerVersion() {
			return ReflectionUtils.SERVER_VERSION;
		}
		/**
		 * Get the items's name from an itemstack
		 * if no name is set then material name is used
		 * @param item
		 * @return item name
		 */
		public static String getItemName(ItemStack item) {
			if (item.hasItemMeta() && item.getItemMeta().hasDisplayName())
				return item.getItemMeta().getDisplayName();
			else {
				String name = "";
				for (String s : item.getType().name().split("_"))
					name = name + s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase() + " ";
				return name.substring(0, name.length() - 1);
			}
		}
		public static double getBalance(Player player) {
			if (Bukkit.getPluginManager().isPluginEnabled("Vault")) {
				RegisteredServiceProvider<Economy> rsp = Bukkit.getServer().getServicesManager()
						.getRegistration(Economy.class);
				if (rsp != null) {
					return rsp.getProvider().getBalance(player);
				}
			}
			return 0;
		}

		public static void takeBalance(Player player, int moneyCost) {
			if (Bukkit.getPluginManager().isPluginEnabled("Vault")) {
				RegisteredServiceProvider<Economy> rsp = Bukkit.getServer().getServicesManager()
						.getRegistration(Economy.class);
				if (rsp != null) {
					rsp.getProvider().withdrawPlayer(player, moneyCost);
					return;
				}
			}
		}
		/**
		 * Fewtch the item' lore from the itemstack
		 * @param item
		 * @return item lore
		 */
		public static List<String> getItemLore(ItemStack item) {
			if (item.hasItemMeta() && item.getItemMeta().hasLore())
				return item.getItemMeta().getLore();
			return new ArrayList<String>();
		}
		@SuppressWarnings("deprecation")
		public static ItemStack getCustomItem(String type, String name, List<String> lore) {
			boolean glow = false;
			if (type.startsWith("<glow>")) {
				glow = true;
				type = type.replace("<glow>", "");
			}
			ItemStack item = EpicMaterials.valueOf(UMaterials.AIR).getItemStack();
			if (type.startsWith("<skull>")) {
				String sname = type.replace("<skull>", "");
				item = EpicMaterials.valueOf(UMaterials.PLAYER_HEAD_ITEM).getItemStack();
				SkullMeta sm = (SkullMeta) item.getItemMeta();
				sm.setOwner(sname);
				item.setItemMeta(sm);
			}
			else
				item = EpicMaterials.valueOf(type.toUpperCase()).getItemStack();
			ItemMeta im = item.getItemMeta();
			if (name != null) {
				im.setDisplayName(ColorUtils.applyColor(name));
			}
			if (lore != null && lore.size() > 0) {
				ArrayList<String> l = new ArrayList<>();
				for (String s : lore)
					l.add(ColorUtils.applyColor(s));
				im.setLore(l);
			}
			else
				im.setLore(new ArrayList<>());
			item.setItemMeta(im);
			if (glow) {
				item.addUnsafeEnchantment(Enchantment.ARROW_INFINITE, 4341);
				ItemMeta ime = item.getItemMeta();
				ime.addItemFlags(new ItemFlag[] { ItemFlag.HIDE_ENCHANTS });
				item.setItemMeta(ime);
			}
			return item;

		}
		/**
		 * Method used for item deserialization from a yml based configurtion section
		 * If an itemstack is already saved at the provided path using <code>ConfigurationSection.setItemstack()</code> then that item is retreived
		 * Else the predefined item parameters are used
		 * The following are loaded & saved in the predefined parameters
		 * -> Item Type (material)
		 * -> Item Display Name
		 * -> Item Lore
		 * -> Glow Effect
		 * -> CustomModelData (1.14 & above) 
		 * -> Custom skull ids (for skulls only)
		 * 
		 * 
		 * this needs the following utilities to function properly
		 * Umaterials (used for itemstack support from 1.8 to 1.15)
		 * SkullCreator (used to generate custom skulls from online ids)
		 * @param node
		 * @param path to the item in the node
		 * @return the fetched itemstack
		 */
		public static ItemStack getItemFromNode(ConfigurationSection node,String path) {
			if (node.isSet(path+".type")) {
				node=node.getConfigurationSection(path);
				ItemStack item=getCustomItem(node);
				if(getServerVersion()>=14&&node.isSet("model")) {
					ItemMeta im=item.getItemMeta();
					im.setCustomModelData(node.getInt("model"));
					item.setItemMeta(im);
				}
				return item;
			}
			return null;
		}
		@SuppressWarnings("deprecation")
		public static ItemStack getCustomItem(ConfigurationSection node) {
			String type = node.getString("type");
			String name = node.getString("name");
			List<String> lore = node.getStringList("lore");
			List<String> flags = node.getStringList("flags");
			Map<Enchantment, Integer> enchants = new HashMap<>();
			boolean glow=false;
			if(type.startsWith("<glow>")) {
				glow=true;
				type=type.substring(6);
			}
			if(node.isSet("enchants")) {
				ConfigurationSection enchantNode = node.getConfigurationSection("enchants");
				for(String key : enchantNode.getKeys(false)) {
					Enchantment enchantment = Enchantment.getByKey(new NamespacedKey(key.split("-")[0], key.split("-")[1]));
					int level = enchantNode.getInt(key);
					enchants.put(enchantment, level);
				}
			}
			ItemStack item = EpicMaterials.valueOf(UMaterials.AIR).getItemStack();
			if(node.isSet("skull-id")) {
				item=SkullCreator.itemFromBase64(node.getString("skull-id"));
			}
			else if(node.isSet("nbt"))
				item = NmsUtils.fromNbtString(node.getString("nbt"));
			else {

				try {
					item = EpicMaterials.valueOf(type.toUpperCase()).getItemStack();
				} catch (Exception e)
				{
					item=new ItemStack(Material.valueOf(type.toUpperCase()));
				}
			}
			ItemMeta im = item.getItemMeta();
			if (name != null) {
				im.setDisplayName(ColorUtils.applyColor(name));
			}
			if (lore != null && lore.size() > 0) {
				ArrayList<String> l = new ArrayList<String>();
				for (String s : lore)
					l.add(ColorUtils.applyColor(s));
				im.setLore(l);
			}
			if(flags!=null && flags.size()>0) {
				for(String flag:flags)
					im.addItemFlags(ItemFlag.valueOf(flag));
			}
			if(enchants!=null && enchants.size()>0) {
				for(Enchantment enchantment:enchants.keySet())
					im.addEnchant(enchantment, enchants.get(enchantment), true);
			}
			if(getServerVersion()>14 && node.isSet("model"))
				im.setCustomModelData(node.getInt("model"));
			if(glow) {
				im.addEnchant(Enchantment.LUCK, 99, true);
				im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
			}
			item.setItemMeta(im);
			return item;
		}
		/**
		 * Save the provided tiemstack to the configuration section
		 * If generic is true then the method <code>ConfigurationSection.setItemstack()</code> is used
		 * Else custom predefined item parameters are used
		 * The following are loaded & saved in the predefined parameters
		 * -> Item Type (material)
		 * -> Item Display Name
		 * -> Item Lore
		 * -> Enchants
		 * -> Flags
		 * -> CustomModelData (1.14 & above) 
		 * -> Custom skull ids (for skulls only)
		 * 
		 * 
		 * this needs the following utilities to function properly
		 * Umaterials (used for itemstack support from 1.8 to 1.15)
		 * SkullCreator (used to generate custom skulls from online ids)
		 * @param section
		 * @param path
		 * @param item
		 * @param generic
		 */
		public static void saveItem(ConfigurationSection section,String path,ItemStack item ,boolean withNBT) {
			String type;
			try {
				type=EpicMaterials.valueOf(item).name();
			} catch (Exception e) {
				type=item.getType().name();
			}
			String name=getItemName(item);
			List<String> lore=new ArrayList<String>();
			if(item.hasItemMeta()&&item.getItemMeta().hasLore())
				lore=item.getItemMeta().getLore();
			Map<Enchantment, Integer> enchants = new HashMap<>();
			if(item.hasItemMeta() && item.getItemMeta().hasEnchants())
				enchants = item.getItemMeta().getEnchants();
			List<String> flags = new ArrayList<>();
			if(item.hasItemMeta() && !item.getItemMeta().getItemFlags().isEmpty())
				item.getItemMeta().getItemFlags().forEach(flag -> flags.add(flag.name()));
			section.set(path+".type", type);
			section.set(path+".name", name);
			section.set(path+".lore", lore);
			section.set(path+".flags", flags);
			for(Enchantment enchant:enchants.keySet())
				section.set(path+".enchants."+enchant.getKey().toString().replace(":", "-"), enchants.get(enchant));
			if(getServerVersion()>=14&&item.hasItemMeta()&&item.getItemMeta().hasCustomModelData()) {
				section.set(path+".model", item.getItemMeta().getCustomModelData());
			}
			if(NmsUtils.getSkullBase64(item)!=null&&!NmsUtils.getSkullBase64(item).equals("")) {
				section.set(path+".skull-id", NmsUtils.getSkullBase64(item));
			}
			if(withNBT) 
				section.set(path+".nbt", NmsUtils.toNBTString(item));
			else
				section.set(path+".nbt", null);
		}
		
		/**
		 * Give an item to the player using vanilla like method
		 * @param player
		 * @param item
		 */
		public static void giveItem(Player player, ItemStack item) {
			if (item == null)
				return;
			int iterations = (int) ((double)item.getAmount()/(double)item.getMaxStackSize());
			if(iterations>1) {
				int spillage=item.getAmount()%item.getMaxStackSize();
				for(int i=0;i<iterations;i++) {
					ItemStack toGive=item.clone();
					toGive.setAmount(item.getMaxStackSize());
					if (player.getInventory().firstEmpty() < 0)
						player.getWorld().dropItem(player.getLocation(), toGive);
					else
						player.getInventory().addItem(toGive);
				}
				if(spillage >=1) {
					ItemStack toGive=item.clone();
					toGive.setAmount(spillage);
					if (player.getInventory().firstEmpty() < 0)
						player.getWorld().dropItem(player.getLocation(), toGive);
					else
						player.getInventory().addItem(toGive);
				}
			}
			else {
				if (player.getInventory().firstEmpty() < 0)
					player.getWorld().dropItem(player.getLocation(), item);
				else
					player.getInventory().addItem(item);
			}
		}
		/**
		 * Fetch the skull of a player
		 * @param player
		 * @return
		 */
		public static ItemStack getSkull(Player player) {
			return getSkull(player.getName());
		}
		/**
		 * Fetch the skull of a player
		 * @param player
		 * @return
		 */
		@SuppressWarnings("deprecation")
		public static ItemStack getSkull(String player) {
			ItemStack skull = EpicMaterials.valueOf(UMaterials.PLAYER_HEAD_ITEM).getItemStack();
			SkullMeta sm = (SkullMeta) skull.getItemMeta();
			sm.setOwner(player);
			sm.setDisplayName(player + "'s head");
			skull.setItemMeta(sm);
			return skull;
		}

		public static float getExperience(Player player) {
			int exp = Math.round(getExpAtLevel(player.getLevel()) * player.getExp());
			int currentLevel = player.getLevel();

			while (currentLevel > 0) {
				currentLevel--;
				exp += getExpAtLevel(currentLevel);
			}
			if (exp < 0) {
				exp = Integer.MAX_VALUE;
			}
			return exp;
		}

		public static void takeExperience(Player player, int xpCost) {
			setTotalExperience(player, (int) (getExperience(player) - xpCost));
		}

		// sets player's Experience
		public static void setTotalExperience(Player player, int exp) {
			player.setExp(0.0F);
			player.setLevel(0);
			player.setTotalExperience(0);

			int amount = exp;
			while (amount > 0) {
				int expToLevel = getExpAtLevel(player.getLevel());
				amount -= expToLevel;
				if (amount >= 0) {

					player.giveExp(expToLevel);
					continue;
				}
				amount += expToLevel;
				player.giveExp(amount);
				amount = 0;
			}
		}
		// extra fuction for above logic
		private static int getExpAtLevel(int level) {
			if (level <= 15) {
				return 2 * level + 7;
			}
			if (level >= 16 && level <= 30) {
				return 5 * level - 38;
			}
			return 9 * level - 158;
		}
		
	}
	
	public static class StringUtils {
		/**
		 * Format a sting to readable text
		 * @param name
		 * @return
		 */
		public static String format(String name) {
			String newName = "";
			for (String s : name.split("_"))
				newName = newName + s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase() + " ";
			return newName.substring(0, newName.length() - 1);
		}
		/**
		 * Format an enum to readable text
		 * @param name
		 * @return
		 */
		public static String format(Enum<?> enumerated) {
			return format(enumerated.name());
		}
		/**
		 * Get block location from a string containing the location
		 * The string is created using the method <code>StringUtils.getBlockString()</code>
		 * @param locationString
		 * @return location of the block
		 */
		public static Location getBlockLocation(String locationString) {
			String[] split = locationString.split(";");
			if(Bukkit.getWorld(split[0])==null)
				Bukkit.createWorld(new WorldCreator(split[0]));
			return new Location(Bukkit.getWorld(split[0]), Integer.valueOf(split[1]), Integer.valueOf(split[2]),
					Integer.valueOf(split[3]));
		}
		/**
		 * Get the string containing the location of the block
		 * Used to store block coords in yml files
		 * @param location
		 * @return
		 */
		public static String getBlockString(Location location) {
			return location.getWorld().getName() + ";" + location.getBlockX() + ";" + location.getBlockY() + ";"
					+ location.getBlockZ();
		}
		/**
		 * Get block location from a string containing the location
		 * The string is created using the method <code>StringUtils.getBlockString()</code>
		 * @param locationString
		 * @return location of the block
		 */
		public static Location getLocation(String locationString) {
			String[] split = locationString.split(";");
			if(Bukkit.getWorld(split[0])==null)
				Bukkit.createWorld(new WorldCreator(split[0]));
			return new Location(Bukkit.getWorld(split[0]), Double.valueOf(split[1].replace("-", ".")), Double.valueOf(split[2].replace("-", ".")),
					Double.valueOf(split[3].replace("-", ".")));
		}
		/**
		 * Get the string containing the location of the block
		 * Used to store block coords in yml files
		 * @param location
		 * @return
		 */
		public static String getString(Location location) {
			String x = location.getX()+"";
			x=x.replace(".", "-");
			String y = location.getY()+"";
			y=y.replace(".", "-");
			String z = location.getZ()+"";
			z=z.replace(".", "-");
			return location.getWorld().getName() + ";" + x + ";" + y + ";"
					+ z;
		}
	}
	
	public static class NmsUtils extends ReflectionUtils {
		
		/**
		 * Method used to forcefully unregister a command from a plugin
		 * @param cmd
		 * @param plugin
		 */
		@SuppressWarnings("unchecked")
		public static void unRegisterCommand(Command command, JavaPlugin plugin) {
			try {
				Object result = getPrivateField(Bukkit.getPluginManager(), "commandMap");
				CommandMap commandMap = (CommandMap) result;
				HashMap<String, Command> knownCommands = (HashMap<String, Command>) getPrivateField(commandMap,
						"knownCommands");
				knownCommands.remove(command.getName());
				for (String alias : command.getAliases()) {
					if (knownCommands.containsKey(alias)
							&& knownCommands.get(alias).toString().contains(plugin.getName())) {
						knownCommands.remove(alias);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		public static Command cloneCommand(PluginCommand command,String newName,List<String> newAliases) {
			return new EpicCommand(newName, newAliases, command.getExecutor(), command.getTabCompleter());
			
//			PluginCommand clone = (PluginCommand) getRefClass(command.getClass()).getConstructor(String.class,Plugin.class).create(newName,command.getPlugin());
//			clone.setAliases(newAliases);
//			clone.setExecutor(command.getExecutor());
//			clone.setTabCompleter(command.getTabCompleter());
//			return clone;
		}
		public static void registerCommand(Command command, JavaPlugin plugin) {
			try {
				
				Object result = getPrivateField(Bukkit.getPluginManager(), "commandMap");
				CommandMap commandMap = (CommandMap) result;
				commandMap.register(plugin.getDescription().getName(), command);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		/**
		 * Method used to replace the executor of one command with the executor of another command
		 * @param from
		 * @param to
		 */
		public static void swapCommands(PluginCommand from,Command to) {
			 swapCommands(from.getName(), from.getAliases(),from.getPlugin().getName(), to);
		}
		@SuppressWarnings("unchecked")
		public static void swapCommands(String name , Collection<String> alisaes,String owningPlugin,Command to) {
			try {
				Object result = getPrivateField(Bukkit.getPluginManager(), "commandMap");
				CommandMap commandMap = (CommandMap) result;
				HashMap<String, Command> knownCommands = (HashMap<String, Command>) getPrivateField(commandMap,
						"knownCommands");
				knownCommands.put(name,to);
				for (String alias : alisaes) {
					if (knownCommands.containsKey(alias)
							&& knownCommands.get(alias).toString().contains(owningPlugin)) {
						knownCommands.put(alias,to);
					}
				}
			} catch (Exception e) {

			}
		}
		private static Object getPrivateField(Object object, String field)
				throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
			Class<?> clazz = object.getClass();
			return getPrivateField(object, field, clazz);
		}
		private static Object getPrivateField(Object object, String field,Class<?> clazz)
				throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
			Field objectField = null;
			try { 
				objectField = clazz.getDeclaredField(field);
			}catch (Exception e) {
				while(clazz.getSuperclass()!=null)
					return getPrivateField(object, field, clazz.getSuperclass());
					
			}
			if(objectField==null)
				return null;
			objectField.setAccessible(true);
			return objectField.get(object);
		}
		/**
		 * Get a base64 key from a skull
		 * Used when storing a skull metadata
		 * @param item
		 * @return
		 */
		public static String getSkullBase64(ItemStack item) {
			String result="";
			try {
				Object nmsItem = getNMSItemStack(item);
				RefClass nbtTagCompound;
				if(USE_NEW_MAPPINGS) {
					nbtTagCompound=getRefClass("{nm}.nbt.NBTTagCompound");
				}
				else
					nbtTagCompound=getRefClass("{nms}.NBTTagCompound");
				RefMethod saveNbt;
				RefMethod getCompound;
				RefMethod getString;
				RefMethod getNbtBase;
				RefMethod getNbtListVal;
				if(USE_NEW_MAPPINGS) {
					if(SEVENTEEN) {
						saveNbt = getRefClass("{nm}.world.item.ItemStack").getMethod("save", nbtTagCompound.getRealClass());
						getCompound = nbtTagCompound.getMethod("getCompound", String.class);
						getNbtBase = nbtTagCompound.getMethod("get", String.class);
						getNbtListVal = getRefClass("{nm}.nbt.NBTTagList").getMethod("get", String.class);
						getString = nbtTagCompound.getMethod("getString", String.class);
					}
					else {
						saveNbt = getRefClass("{nm}.world.item.ItemStack").getMethod("b", nbtTagCompound.getRealClass());
						getCompound = nbtTagCompound.getMethod("p", String.class);
						getNbtBase = nbtTagCompound.getMethod("c", String.class);
						getNbtListVal = getRefClass("{nm}.nbt.NBTTagList").getMethod("k", String.class);
						getString = nbtTagCompound.getMethod("l", String.class);
					}
				}
				else {
					saveNbt = getRefClass("{nms}.ItemStack").getMethod("save",
							nbtTagCompound.getRealClass());
					getCompound = nbtTagCompound.getMethod("getCompound", String.class);
					getNbtBase = nbtTagCompound.getMethod("get", String.class);
					getNbtListVal = getRefClass("{nms}.NBTTagList").getMethod("get", String.class);
					getString = nbtTagCompound.getMethod("getString", String.class);
				}
				Object tag = nbtTagCompound.getConstructor();
				saveNbt.of(nmsItem).call(tag);
				tag = getCompound.of(tag).call("tag");
				tag = getCompound.of(tag).call("SkullOwner");
				tag = getCompound.of(tag).call("Properties");
				Object base = getNbtBase.of(tag).call("textures");
				Object base64 = getNbtListVal.of(base).call(0);
				result =  (String) getString.of(base64).call("Value");
				
				
			}catch (Exception e) {
			}
			return result;
		}
	}
	
	public static class NumericsUtils {
		
		private static final Random RANDOM=new Random();
		@SuppressWarnings({ "unchecked", "rawtypes" })
		private static final NavigableMap<Long, String> suffixes = new TreeMap();
		static {
			suffixes.put(1_000L, "k");
			suffixes.put(1_000_000L, "M");
			suffixes.put(1_000_000_000L, "G");
			suffixes.put(1_000_000_000_000L, "T");
			suffixes.put(1_000_000_000_000_000L, "P");
			suffixes.put(1_000_000_000_000_000_000L, "E");
		}
		/**
		 * format a large number with prefixes like K(kilo), M(mega) G(giga) etc
		 * @param value
		 * @return
		 */
		public static String formatPrefixedNumbers(long value) {
			// Long.MIN_VALUE == -Long.MIN_VALUE so we need an adjustment here
			if (value == Long.MIN_VALUE)
				return formatPrefixedNumbers(Long.MIN_VALUE + 1);
			if (value < 0)
				return "-" + formatPrefixedNumbers(-value);
			if (value < 1000)
				return Long.toString(value); // deal with easy case

			Entry<Long, String> e = suffixes.floorEntry(value);
			Long divideBy = e.getKey();
			String suffix = e.getValue();

			long truncated = value / (divideBy / 10); // the number part of the output times 10
			boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
			return hasDecimal ? (truncated / 10d) + suffix : (truncated / 10) + suffix;
		}
		
		/**
		 * Format a number with 2 decimal spaces
		 * @param amount
		 * @return
		 */
		public static String formatTwoDecimals(long amount) {
			NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.US);
			formatter.setGroupingUsed(true);
			String formatted = formatter.format(amount).replace("$", "");
			return formatted.substring(0, formatted.length() - 3);
		}

		/**
		 * Get a random chance for some action
		 * @param max
		 * @param chance
		 * @return
		 */
		public static boolean getChance(int max, int chance) {
			int d = RANDOM.nextInt(max) + 1;
			if (d <= chance)
				return true;
			return false;
		}
		
		// used to evaluate arithematic expressions & get a double value (this is too
		// usefull)
		// expressions like (2*100)/50 etc
		public static double evaluateString(String str) {
			return new Object() {
				int pos = -1, ch;

				void nextChar() {
					ch = (++pos < str.length()) ? str.charAt(pos) : -1;
				}

				boolean eat(int charToEat) {
					while (ch == ' ')
						nextChar();
					if (ch == charToEat) {
						nextChar();
						return true;
					}
					return false;
				}

				double parse() {
					nextChar();
					double x = parseExpression();
					if (pos < str.length())
						throw new RuntimeException("Unexpected: " + (char) ch);
					return x;
				}

				// Grammar:
				// expression = term | expression `+` term | expression `-` term
				// term = factor | term `*` factor | term `/` factor
				// factor = `+` factor | `-` factor | `(` expression `)`
				// | number | functionName factor | factor `^` factor

				double parseExpression() {
					double x = parseTerm();
					for (;;) {
						if (eat('+'))
							x += parseTerm(); // addition
						else if (eat('-'))
							x -= parseTerm(); // subtraction
						else
							return x;
					}
				}

				double parseTerm() {
					double x = parseFactor();
					for (;;) {
						if (eat('*'))
							x *= parseFactor(); // multiplication
						else if (eat('/'))
							x /= parseFactor(); // division
						else
							return x;
					}
				}

				double parseFactor() {
					if (eat('+'))
						return parseFactor(); // unary plus
					if (eat('-'))
						return -parseFactor(); // unary minus

					double x;
					int startPos = this.pos;
					if (eat('(')) { // parentheses
						x = parseExpression();
						eat(')');
					} else if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
						while ((ch >= '0' && ch <= '9') || ch == '.')
							nextChar();
						x = Double.parseDouble(str.substring(startPos, this.pos));
					} else if (ch >= 'a' && ch <= 'z') { // functions
						while (ch >= 'a' && ch <= 'z')
							nextChar();
						String func = str.substring(startPos, this.pos);
						x = parseFactor();
						if (func.equals("sqrt"))
							x = Math.sqrt(x);
						else if (func.equals("sin"))
							x = Math.sin(Math.toRadians(x));
						else if (func.equals("cos"))
							x = Math.cos(Math.toRadians(x));
						else if (func.equals("tan"))
							x = Math.tan(Math.toRadians(x));
						else
							throw new RuntimeException("Unknown function: " + func);
					} else {
						throw new RuntimeException("Unexpected: " + (char) ch);
					}

					if (eat('^'))
						x = Math.pow(x, parseFactor()); // exponentiation

					return x;
				}
			}.parse();
		}
		
	}

	

	
	
}
