package me.backstabber.epicsetspawners.utils.materials;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;

public interface Versionable {
    String VERSION = Bukkit.getVersion();
	public static boolean EIGHT = VERSION.contains("1.8"), 
		NINE = VERSION.contains("1.9"), 
		TEN = VERSION.contains("1.10"), 
		ELEVEN = VERSION.contains("1.11"), 
		TWELVE = VERSION.contains("1.12"), 
		THIRTEEN = VERSION.contains("1.13"), 
		FOURTEEN = VERSION.contains("1.14"), 
		FIFTEEN = VERSION.contains("1.15"), 
		SIXTEEN=VERSION.contains("1.16") ,
		SEVENTEEN=VERSION.contains("1.17"),
		EIGHTEEN=VERSION.contains("1.18"),
		NINETEEN=VERSION.contains("1.19");
	public static boolean USE_DEFAULT_MATERIALS = SIXTEEN || SEVENTEEN || EIGHTEEN || NINETEEN;
	public static boolean USE_RGB_COLORS = SIXTEEN || SEVENTEEN || EIGHTEEN || NINETEEN;
	public static boolean USE_NEW_MAPPINGS = SEVENTEEN || EIGHTEEN || NINETEEN;
	public static double SERVER_VERSION=getVersion();;
	static Double getVersion() {
		Pattern VERSION_PATTERN = Pattern.compile("minecraftVersion=1.[0-9.]+");
		String versionString=Bukkit.getServer().toString();
		Matcher match = VERSION_PATTERN.matcher(versionString);
		if(match.find()) {
			String result=versionString.substring(match.start()+19,match.end());
			return Double.valueOf(result);
		}
		return 19D;
	}
}
