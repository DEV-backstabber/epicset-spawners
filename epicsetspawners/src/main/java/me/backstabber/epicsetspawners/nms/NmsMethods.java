package me.backstabber.epicsetspawners.nms;


import org.bukkit.block.Block;
import me.backstabber.epicsetspawners.hooks.StackersHook;
import me.backstabber.epicsetspawners.utils.ReflectionUtils;

public abstract class NmsMethods extends ReflectionUtils {
	protected StackersHook hook;

	public NmsMethods(StackersHook hook) {
		this.hook = hook;
	}
	public abstract void addNBTTags(Block block, String entityId, String spawnData, int delay, int minDelay,
			int maxDelay, int spawnRange, int amount);
	/**
	 * Psuedo Code for above method
	 * 1) Set creature for spawner (in 1.8-1.10 use api) for rest use tag entityId
	 * 2) 
	 */
}
