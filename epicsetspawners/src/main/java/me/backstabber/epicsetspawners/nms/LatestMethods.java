package me.backstabber.epicsetspawners.nms;

import me.backstabber.epicsetspawners.hooks.StackersHook;

public abstract class LatestMethods extends NmsMethods {

	public LatestMethods(StackersHook hook) {
		super(hook);
	}
}
