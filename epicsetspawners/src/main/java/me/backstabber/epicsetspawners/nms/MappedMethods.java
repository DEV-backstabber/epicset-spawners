package me.backstabber.epicsetspawners.nms;


import org.bukkit.Location;
import org.bukkit.block.Block;

import me.backstabber.epicsetspawners.hooks.StackersHook;

public class MappedMethods extends NmsMethods {

	public MappedMethods(StackersHook hook) {
		super(hook);
	}
	protected static RefMethod getHandle = getRefClass("{cb}.CraftWorld").getMethod("getHandle");
	protected static RefConstructor BlockPosition = getRefClass("{nm}.core.BlockPosition").getConstructor(int.class,
			int.class, int.class);
	protected static RefConstructor NBTTagCompound = getRefClass("{nm}.nbt.NBTTagCompound").getConstructor();
	protected static RefMethod getTileEntity = (SERVER_VERSION <=17)
			?getRefClass("{nm}.world.level.World").getMethod("getTileEntity",getRefClass("{nm}.core.BlockPosition").getRealClass(),boolean.class) 
			:getRefClass("{nm}.world.level.World").getMethod("getBlockEntity", getRefClass("{nm}.core.BlockPosition").getRealClass(),boolean.class);
	protected static RefMethod save = (SERVER_VERSION <=17)
			?getRefClass("{nm}.world.level.block.entity.TileEntityMobSpawner").getMethod("save",getRefClass("{nm}.nbt.NBTTagCompound").getRealClass()) 
			:getRefClass("{nm}.world.level.block.entity.TileEntityMobSpawner").getMethod("b", getRefClass("{nm}.nbt.NBTTagCompound").getRealClass());
	protected static RefMethod load = (SERVER_VERSION <=17)
			?getRefClass("{nm}.world.level.block.entity.TileEntityMobSpawner").getMethod("load",getRefClass("{nm}.nbt.NBTTagCompound").getRealClass()) 
			:getRefClass("{nm}.world.level.block.entity.TileEntityMobSpawner").getMethod("a", getRefClass("{nm}.nbt.NBTTagCompound").getRealClass());
	
	protected static RefMethod parse = (SERVER_VERSION <=17)
			?getRefClass("{nm}.nbt.MojangsonParser").getMethod("parse", String.class)
			:getRefClass("{nm}.nbt.MojangsonParser").getMethod("a", String.class);
	
	protected static RefMethod remove = (SERVER_VERSION <=17)
			?getRefClass("{nm}.nbt.NBTTagCompound").getMethod("remove", String.class)
			:getRefClass("{nm}.nbt.NBTTagCompound").getMethod("r", String.class);
	
	protected static RefMethod set = (SERVER_VERSION <=17)
			?getRefClass("{nm}.nbt.NBTTagCompound").getMethod("set", String.class,getRefClass("{nm}.nbt.NBTBase").getRealClass())
			:getRefClass("{nm}.nbt.NBTTagCompound").getMethod("a", String.class,getRefClass("{nm}.nbt.NBTBase").getRealClass());
	
	protected static RefMethod setString = (SERVER_VERSION <=17)
			?getRefClass("{nm}.nbt.NBTTagCompound").getMethod("setString", String.class,String.class)
			:getRefClass("{nm}.nbt.NBTTagCompound").getMethod("a", String.class,String.class);
	
	protected static RefMethod setShort = (SERVER_VERSION <=17)
			?getRefClass("{nm}.nbt.NBTTagCompound").getMethod("setShort", String.class,short.class)
			:getRefClass("{nm}.nbt.NBTTagCompound").getMethod("a", String.class,short.class);

	
	protected static RefConstructor NBTTagList = getRefClass("{nm}.nbt.NBTTagList").getConstructor();
	protected static RefMethod setListValue =getRefClass("{nm}.nbt.NBTTagList").getMethod("b", int.class,getRefClass("{nm}.nbt.NBTBase").getRealClass());
	
	
	@Override
	public void addNBTTags(Block block, String entityId, String spawnData, int delay, int minDelay, int maxDelay,
			int spawnRange, int amount) {

		// set item spawner data
		Location location = block.getLocation();

		Object blockPos = BlockPosition.create(location.getBlockX(), location.getBlockY(), location.getBlockZ());
		Object spawner = getTileEntity.of(getHandle.of(location.getWorld()).call()).call(blockPos,true);
		Object tag = NBTTagCompound.create();
		save.of(spawner).call(tag);
		// setting basic data using nbt tags
		if (hook.isEntityEnabled())
			setShort.of(tag).call("SpawnCount", (short) 1);
		else
			setShort.of(tag).call("SpawnCount", (short) amount);
		setShort.of(tag).call("Delay", (short) delay);
		setShort.of(tag).call("MaxSpawnDelay", (short) maxDelay);
		setShort.of(tag).call("MinSpawnDelay", (short) minDelay);
		setShort.of(tag).call("SpawnRange", (short) 5);
		setShort.of(tag).call("MaxNearbyEntities", (short) 5);
		setShort.of(tag).call("RequiredPlayerRange", (short) spawnRange);
		// set mob type & extra SpawnData
		Object spawnDataTag = NBTTagCompound.create();
		try {
			spawnDataTag = parse.call(spawnData);
		} catch (Exception e) {

		}
		Object itemTag = NBTTagCompound.create();
		if (entityId.equalsIgnoreCase("item")) {
			remove.of(tag).call("SpawnPotentials");
			Object entityTag = NBTTagCompound.create();
			setString.of(entityTag).call("id","item");
			set.of(entityTag).call("Item",spawnDataTag);
			set.of(itemTag).call("entity",entityTag);
			set.of(tag).call("SpawnData", itemTag);
		} else {
			setString.of(spawnDataTag).call("id", "minecraft:" + entityId.toLowerCase() + "");
			remove.of(spawnDataTag).call("Pos");
			remove.of(spawnDataTag).call("UUID");
			Object entityTag = NBTTagCompound.create();
			set.of(entityTag).call("entity",spawnDataTag);
			set.of(tag).call("SpawnData", entityTag);
			set.of(tag).call("SpawnPotentials", spawnDataTag);
		}
		load.of(spawner).call(tag);

	}
}
