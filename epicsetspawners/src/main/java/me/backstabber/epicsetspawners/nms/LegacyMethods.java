package me.backstabber.epicsetspawners.nms;

import me.backstabber.epicsetspawners.hooks.StackersHook;
public abstract class LegacyMethods extends NmsMethods {

	public LegacyMethods(StackersHook hook) {
		super(hook);
	}
}
