package me.backstabber.epicsetspawners.listeners;

import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.SpawnerSpawnEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import me.backstabber.epicsetspawners.Dependable;
import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder;
import me.backstabber.epicsetspawners.api.builder.SpawnerBuilder.SpawnerType;
import me.backstabber.epicsetspawners.api.data.SpawnerData;
import me.backstabber.epicsetspawners.api.data.SpawnerData.SpawnerParameters;
import me.backstabber.epicsetspawners.api.events.SpawnerPlaceEvent;
import me.backstabber.epicsetspawners.api.events.SpawnerStackUpEvent;
import me.backstabber.epicsetspawners.api.events.StackedBlockBreakEvent;
import me.backstabber.epicsetspawners.data.EpicSpawnerData;
import me.backstabber.epicsetspawners.utils.CommonUtils;
import me.backstabber.epicsetspawners.utils.ReflectionUtils;
import me.backstabber.epicsetspawners.utils.materials.EpicMaterials;
import me.backstabber.epicsetspawners.utils.materials.UMaterials;

public class BlockListener extends Dependable implements Listener {


//	@Inject
//	private EpicSetSpawners plugin;
//	@Inject
//	private LocationStore locations;
//	@Inject
//	private SpawnerStore store;
//	@Inject
//	private StackedBlocks blocks;
//	@Inject
//	private StackersHook stackerHook;

	public BlockListener(EpicSetSpawners plugin) {
		super(plugin);
	}
	@EventHandler
	public void onSpawnerPlaced(SpawnerPlaceEvent event) {
		if(event.isCancelled() || event.getSpawner()==null)
			return;
		if(((int)event.getSpawner().getParameter(SpawnerParameters.AMOUNT))<=1)
			return;
		if(!plugin.getSettings().getFile().getBoolean("settings.spawner-stacking.enabled")) {
			event.getSpawner().changeParameter(SpawnerParameters.AMOUNT, 1);
		}
		if(((int)event.getSpawner().getParameter(SpawnerParameters.AMOUNT))>plugin.getSettings().getFile().getInt("settings.spawner-stacking.max-stack")) {
			event.getSpawner().changeParameter(SpawnerParameters.AMOUNT, plugin.getSettings().getFile().getInt("settings.spawner-stacking.max-stack"));
		}
	}
	@EventHandler
	public void onSpawnerStackUp(SpawnerStackUpEvent event) {
		if(event.isCancelled() || event.getSpawner()==null)
			return;
		if(((int)event.getSpawner().getParameter(SpawnerParameters.AMOUNT))<=1)
			return;
		if(!plugin.getSettings().getFile().getBoolean("settings.spawner-stacking.enabled")) {
			event.setCancelled(true);
		}
		if(((int)event.getSpawner().getParameter(SpawnerParameters.AMOUNT))>plugin.getSettings().getFile().getInt("settings.spawner-stacking.max-stack")) {
			event.getSpawner().changeParameter(SpawnerParameters.AMOUNT, plugin.getSettings().getFile().getInt("settings.spawner-stacking.max-stack"));
		}
	}
	
	
	
	
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGH)
	public void onBlockPlace(BlockPlaceEvent event) {
		if(event.isCancelled())
			return;
		ItemStack item = event.getPlayer().getInventory().getItemInHand();
		if (item == null || item.getType().equals(EpicMaterials.valueOf("AIR").getMaterial()))
			return;
		String tag = ReflectionUtils.getNbtTag(item, "EpicSpawner");
		if (tag == null || tag.equals("")) {
			if(item.getType().equals(EpicMaterials.valueOf(UMaterials.SPAWNER).getMaterial())) {
				SpawnerData spawner = spawnerStore.getSpawner(EntityType.PIG);
				((EpicSpawnerData) spawner).placeBlock(event);
			}
			return;
		}
		JsonParser parser=new JsonParser();
		JsonObject json=(JsonObject) parser.parse(tag);
		if(json.get("EpicSetSpawner")!=null) {
			SpawnerType type=SpawnerType.valueOf(json.get("Type").getAsString().toUpperCase());
			String name=json.get("Name").getAsString();
			try {
			SpawnerData spawner=SpawnerBuilder.load(type, name).getSpawner(json);
			((EpicSpawnerData) spawner).placeBlock(event);
			} catch(IllegalArgumentException e) {
				plugin.getLogger().log(Level.WARNING, event.getPlayer().getName()+" tried to place an unrecognized spawner.");
				SpawnerData spawner=spawnerStore.getSpawner(EntityType.PIG);
				((EpicSpawnerData) spawner).placeBlock(event);
			}
			
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGH)
	public void onBlockBreak(BlockBreakEvent event) {
		if(event instanceof StackedBlockBreakEvent || event.isCancelled())
			return;
		Block block = event.getBlock();
		if(stackedBlocks.isStored(block)) {
			//create event
			int stackAmount = stackedBlocks.fetch(block);
			if(stackAmount<=0) {
				stackedBlocks.removeBlock(block);
				return;
			}
			event.setCancelled(true);
			StackedBlockBreakEvent stackedEvent = new StackedBlockBreakEvent(block, event.getPlayer(), stackAmount, 1);
			Bukkit.getPluginManager().callEvent(stackedEvent);
			if(stackedEvent.isCancelled())
				return;
			int newAmount = stackedEvent.getStackAmount()-stackedEvent.getBreakAmount();
			for(int count = 0;count<stackedEvent.getBreakAmount();count++) {
				for(ItemStack drop:block.getDrops(event.getPlayer().getInventory().getItemInHand()))
					block.getWorld().dropItemNaturally(block.getLocation(), drop);
			}
			if(newAmount<=0) {
				stackedBlocks.removeBlock(block);
				block.setType(Material.AIR);
			}
			else
				stackedBlocks.setBlock(block, newAmount);
		}
		if (!locationStore.isStored(block)) {
			if(block.getState() instanceof CreatureSpawner) {
				CreatureSpawner cSpawner = (CreatureSpawner) block.getState();
				SpawnerData spawner = spawnerStore.getSpawner(cSpawner.getSpawnedType());
				((EpicSpawnerData) spawner).breakBlock(event);
			}
			return;
		}
		try {
		SpawnerData spawner = locationStore.fetch(block);
		((EpicSpawnerData) spawner).breakBlock(event);
		}catch(IllegalArgumentException e) {
			plugin.getLogger().log(Level.WARNING, "Unknown spawner found at "+CommonUtils.StringUtils.getBlockString(block.getLocation()));
			SpawnerData spawner=spawnerStore.getSpawner(EntityType.PIG);
			((EpicSpawnerData) spawner).breakBlock(event);
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onBlockExplode(EntityExplodeEvent event) {
		if(event.isCancelled())
			return;
		for (Block block : event.blockList()) {
			if (locationStore.isStored(block)) {
				SpawnerData spawner = locationStore.fetch(block);
				((EpicSpawnerData) spawner).blockExplode(block);
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onSpawnerSpawn(SpawnerSpawnEvent event) {
		if(event.isCancelled())
			return;
		Block block = event.getSpawner().getBlock();
		Entity entity = event.getEntity();
		if (!locationStore.isStored(block))
			return;
		SpawnerData spawner = locationStore.fetch(block);
		if (!spawner.isEnabled()) {
			event.setCancelled(true);
			return;
		}
		if (spawner instanceof EpicSpawnerData) {
			if(!((EpicSpawnerData)spawner).canSpawn(block, event.getEntity())) {
				event.getEntity().remove();
				return;
			}
			if(spawner.getType().equals(SpawnerType.CUSTOM_MOB))
				return;
			int amount = ((EpicSpawnerData) spawner).getAmount();
			if (entity instanceof Item && stackersHook.isItemEnabled())
				stackersHook.setStackSize(entity, amount);
			else if (entity instanceof LivingEntity && stackersHook.isEntityEnabled())
				stackersHook.setStackSize(entity, amount);
		}
	}
	@EventHandler
	public void onSpawnerChange(PlayerInteractEvent event) {
		if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
		{
			Block block=event.getClickedBlock();
			ItemStack item=event.getItem();
			if(item!=null&&item.getType().equals(EpicMaterials.valueOf(UMaterials.SPAWN_EGG).getMaterial())&&block.getState() instanceof CreatureSpawner){
				event.setCancelled(true);
			}
			if(event.getPlayer().isSneaking()&&Bukkit.getPluginManager().isPluginEnabled("WildStacker")&&block.getState() instanceof CreatureSpawner) {
				new BukkitRunnable() {
					@Override
					public void run() {
						event.getPlayer().closeInventory();
					}
				}.runTaskLater(plugin, 1);
			}
		}
	}
}
