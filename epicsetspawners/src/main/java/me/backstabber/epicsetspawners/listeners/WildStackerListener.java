package me.backstabber.epicsetspawners.listeners;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import com.bgsoftware.wildstacker.api.events.SpawnerPlaceEvent;

import me.backstabber.epicsetspawners.Dependable;
import me.backstabber.epicsetspawners.EpicSetSpawners;

public class WildStackerListener extends Dependable implements Listener {
	

//	@Inject
//	private EpicSetSpawners plugin;
//	@Inject
//	private LocationStore locations;
	public WildStackerListener(EpicSetSpawners plugin) {
		super(plugin);
	}
	@EventHandler
	public void onPlace(SpawnerPlaceEvent event) {
		event.setCancelled(true);
		Block block=event.getSpawner().getLocation().getBlock();
		if(locationStore.isStored(block)) {
			new BukkitRunnable() {
				@Override
				public void run() {
					locationStore.fetch(block).applyToBlock(block, null);
				}
			}.runTaskLater(plugin, 2);
		}
	}
}
