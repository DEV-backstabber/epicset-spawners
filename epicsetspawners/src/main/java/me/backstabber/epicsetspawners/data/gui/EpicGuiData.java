package me.backstabber.epicsetspawners.data.gui;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;


import me.backstabber.epicsetspawners.api.builder.validate.GuiPaths;
import me.backstabber.epicsetspawners.api.data.GuiData;
import me.backstabber.epicsetspawners.api.data.SpawnerData;
import me.backstabber.epicsetspawners.api.data.UpgradeData;
import me.backstabber.epicsetspawners.data.EpicSpawnerData;
import me.backstabber.epicsetspawners.data.upgrade.EpicUpgradeData;
import me.backstabber.epicsetspawners.utils.ColorUtils;
import me.backstabber.epicsetspawners.utils.CommonUtils;
import me.backstabber.epicsetspawners.utils.ReflectionUtils;
import me.backstabber.epicsetspawners.utils.materials.EpicMaterials;
import me.backstabber.epicsetspawners.utils.materials.UMaterials;

public class EpicGuiData implements GuiData {

	private SpawnerData spawner;
	private FileConfiguration parentFile;

	private boolean enabled;
	private Inventory gui;
	private Player viewer;
	private Block block;
	public EpicGuiData (SpawnerData spawner,FileConfiguration parent) {
		this.spawner=spawner;
		this.parentFile=parent;
		init();
	}
	public FileConfiguration getParent() {
		return parentFile;
	}
	@Override
	public boolean isEnabled() {
		return enabled;
	}
	@Override
	public boolean hasGui() {
		return enabled;
	}

	@Override
	public void openGui(Player player,Block block) {
		if (viewer != null) {

			viewer.closeInventory();
			viewer = null;
		}
		init();
		viewer = player;
		this.block=block;
		player.openInventory(gui);
	}
	
	@Override
	public boolean isGuiOpen() {
		return (viewer != null);
	}

	@Override
	public Player getViewer() {
		return viewer;
	}

	public void handleClick(InventoryClickEvent clickEvent) {
		if (clickEvent.getRawSlot() < 0)
			return;
		Inventory inv = clickEvent.getClickedInventory();
		Player player = (Player) clickEvent.getWhoClicked();
		if (viewer != null && player.equals(viewer) && inv.equals(gui)) {
			clickEvent.setCancelled(true);
			ItemStack item = clickEvent.getCurrentItem();
			if (item == null || item.getType().equals(EpicMaterials.valueOf(UMaterials.AIR).getMaterial()))
				return;
			String tag = ReflectionUtils.getNbtTag(item,"GuiTriggers");
			if (tag == null || tag.equals(""))
				return;
			GuiTriggers trigger = GuiTriggers.valueOf(tag);
			switch (trigger) {

			case TOGGLE_SPAWNER:
				spawner.setEnabled(!spawner.isEnabled());
				init();
				break;

			case CLOSE_GUI:
				player.closeInventory();
				this.viewer = null;
				break;

			case UPGRADE_SPAWNER:
				UpgradeData upgrade = spawner.getUpgradeData();
				if(((EpicUpgradeData) upgrade).upgrade(player))
					((EpicSpawnerData)spawner).save(block);
					init();
				break;
			case NONE:
				return;

			default:
				break;
			}
		}
	}

	public void handleClose(InventoryCloseEvent closeEvent) {

		if (this.viewer == null)
			return;
		Player player = (Player) closeEvent.getPlayer();
		Inventory inv = closeEvent.getInventory();
		if (player.equals(viewer) && inv.equals(gui))
			this.viewer = null;

	}

	private void init() {
		this.enabled = parentFile.getBoolean(GuiPaths.ENABLED.getPath());
		if (this.gui == null)
			this.gui = Bukkit.createInventory(null, 9 * parentFile.getInt(GuiPaths.GUI_SIZE.getPath()),
					ColorUtils.applyColor(parentFile.getString(GuiPaths.GUI_NAME.getPath())));
		String iname = ((EpicSpawnerData) spawner)
				.replacePlaceholders(parentFile.getString(GuiPaths.BACKGROUND_NAME.getPath()));
		List<String> ilore = ((EpicSpawnerData) spawner)
				.replacePlaceholders(parentFile.getStringList(GuiPaths.BACKGROUND_LORE.getPath()));
		ItemStack background = CommonUtils.BukkitUtils
				.getCustomItem(parentFile.getString(GuiPaths.BACKGROUND_TYPE.getPath()), iname, ilore);
		background = ReflectionUtils.addNbtTag(background, "GuiTriggers", GuiTriggers.NONE.name());
		for (int index = 0; index < gui.getSize(); index++) {
			if (!parentFile.isSet(GuiPaths.ITEMS.getPath() + "." + index))
				gui.setItem(index, background);
			else {

				ConfigurationSection section = parentFile
						.getConfigurationSection(GuiPaths.ITEMS.getPath() + "." + index);
				GuiTriggers trigger = GuiTriggers.valueOf(section.getString("trigger").toUpperCase());
				if(trigger.equals(GuiTriggers.UPGRADE_SPAWNER)) {
					if(!spawner.getUpgradeData().isUpgradable()||spawner.getUpgradeData().isMaxed()) {
						gui.setItem(index, background);
						continue;
					}
				}
				iname = ((EpicSpawnerData) spawner).replacePlaceholders(section.getString("name"));
				ilore = ((EpicSpawnerData) spawner).replacePlaceholders(section.getStringList("lore"));
				ItemStack item = CommonUtils.BukkitUtils.getCustomItem(section.getString("type"), iname, ilore);
				item = ReflectionUtils.addNbtTag(item, "GuiTriggers", trigger.name());
				gui.setItem(index, item);
			}
		}

	}
}
