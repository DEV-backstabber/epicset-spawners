package me.backstabber.epicsetspawners.data.upgrade;

import org.bukkit.entity.Player;

import me.backstabber.epicsetspawners.EpicSetSpawners;
import me.backstabber.epicsetspawners.api.data.SpawnerData;
import me.backstabber.epicsetspawners.api.data.SpawnerData.SpawnerParameters;
import me.backstabber.epicsetspawners.utils.ColorUtils;
import me.backstabber.epicsetspawners.utils.CommonUtils;

public class UpgradeCosts {
	/*
	 * this object will only act as a storage for upgrade Costs
	 */
	private SpawnerData spawner;
	private int moneyCost;
	private int xpCost;

	public UpgradeCosts(SpawnerData spawner,int moneyCost, int xpCost) {
		this.moneyCost = moneyCost;
		this.xpCost = xpCost;
		this.spawner=spawner;
	}

	public boolean hasCosts(Player player) {
		EpicSetSpawners plugin=EpicSetSpawners.getPlugin(EpicSetSpawners.class);
		int amount=(int) spawner.getParameter(SpawnerParameters.AMOUNT);
		if (CommonUtils.BukkitUtils.getExperience(player) >= xpCost*amount && CommonUtils.BukkitUtils.getBalance(player) >= moneyCost*amount)
			return true;

		String msg=plugin.getSettings().getFile().getString("message.upgrade-fail");
		if(CommonUtils.BukkitUtils.getExperience(player) < xpCost*amount && CommonUtils.BukkitUtils.getBalance(player) < moneyCost*amount) 
			msg=msg.replace("%cost%", "money & experience");
		else if(CommonUtils.BukkitUtils.getExperience(player) < xpCost*amount)
			msg=msg.replace("%cost%", "experience");
		else
			msg=msg.replace("%cost%", "money");
		player.sendMessage(ColorUtils.applyColor(msg));
		return false;
	}

	public void deductCosts(Player player) {
		if (hasCosts(player)) {
			int amount=(int) spawner.getParameter(SpawnerParameters.AMOUNT);
			CommonUtils.BukkitUtils.takeExperience(player, xpCost*amount);
			CommonUtils.BukkitUtils.takeBalance(player, moneyCost*amount);
		}
	}

	public String replaceCosts(String s) {
		int amount=(int) spawner.getParameter(SpawnerParameters.AMOUNT);
		s = s.replace("%money%", CommonUtils.NumericsUtils.formatTwoDecimals(moneyCost*amount));
		s = s.replace("%exp%", CommonUtils.NumericsUtils.formatTwoDecimals(xpCost*amount));
		return s;
	}
}
